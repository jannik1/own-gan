import pandas

import numpy as np
import tensorflow as tf

from src.example import run_example
from src.generator import sample_Z
import matplotlib.pyplot as plt

# vocabulary = ["set", "a", "reminder", "for", "football", "tomorrow", "<pad>", "<eos>", "today", "yoga", "swimming"]
# output = [[], [], [], [], []]
# output[0] = [6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 0, 1, 2, 3, 4, 5, 7]
# output[1] = [6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 0, 1, 2, 3, 4, 8, 7]
# output[2] = [6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 0, 1, 2, 3, 9, 5, 7]
# output[3] = [6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 0, 1, 2, 3, 9, 8, 7]
# output[4] = [6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 0, 1, 2, 3, 10, 8, 7]
from src.util import load_vocab_inputs, mode, kb, get_batch_size, get_ng_steps, get_nd_steps, sample_data, \
    get_inputs_int, get_vocab_len, to_plot, tensor_size, num_hidden, num_classes, num_input, BiRNNGenerator, BiRNNDiscriminator


def save_and_plot():
    saver.save(sess, "../weights/model_" + mode + kb + "_" + str(i) + ".ckpt")
    g_plot = sess.run(G_sample, feed_dict={Z: Z_batch})

    dialogues = run_example(g_plot)
    for dialogue in dialogues:
        print(dialogue)
    df = pandas.DataFrame(dialogues)
    df.to_csv("generated-dialogs-" + mode + kb + "_" + str(i) + ".csv", sep=";")

    if to_plot:
        plt.figure()
        for k in range(len(inputs_int[0]) + 1):
            xax = plt.scatter(x_plot[:, 0], x_plot[:, k])
            gax = plt.scatter(g_plot[:, 0], g_plot[:, k])

            plt.legend((xax, gax), ("Real Data", "Generated Data"))
            plt.title('Samples at Iteration %d' % i)
            plt.tight_layout()
            plt.savefig("../plots/iterations/iteration_%d-" % i + "_position_" + str(k) + mode + ".png")
            plt.close()

        plt.figure()
        rrd = plt.scatter(rrep_dstep[:, 0], rrep_dstep[:, 1], alpha=0.5)
        rrg = plt.scatter(rrep_gstep[:, 0], rrep_gstep[:, 1], alpha=0.5)
        grd = plt.scatter(grep_dstep[:, 0], grep_dstep[:, 1], alpha=0.5)
        grg = plt.scatter(grep_gstep[:, 0], grep_gstep[:, 1], alpha=0.5)

        plt.legend((rrd, rrg, grd, grg), ("Real Data Before G step", "Real Data After G step",
                                          "Generated Data Before G step", "Generated Data After G step"))
        plt.title('Transformed Features at Iteration %d' % i)
        plt.tight_layout()
        plt.savefig("../plots/features/feature_transform_%d-" % i + mode + ".png")
        plt.close()

        plt.figure()

        rrdc = plt.scatter(np.mean(rrep_dstep[:, 0]), np.mean(rrep_dstep[:, 1]), s=100, alpha=0.5)
        rrgc = plt.scatter(np.mean(rrep_gstep[:, 0]), np.mean(rrep_gstep[:, 1]), s=100, alpha=0.5)
        grdc = plt.scatter(np.mean(grep_dstep[:, 0]), np.mean(grep_dstep[:, 1]), s=100, alpha=0.5)
        grgc = plt.scatter(np.mean(grep_gstep[:, 0]), np.mean(grep_gstep[:, 1]), s=100, alpha=0.5)

        plt.legend((rrdc, rrgc, grdc, grgc), ("Real Data Before G step", "Real Data After G step",
                                              "Generated Data Before G step", "Generated Data After G step"))

        plt.title('Centroid of Transformed Features at Iteration %d' % i)
        plt.tight_layout()
        plt.savefig("../plots/features/feature_transform_centroid_%d-" % i + mode + ".png")
        plt.close()


if __name__ == "__main__":
    load_vocab_inputs()
    batch_size = get_batch_size()
    nd_steps = get_nd_steps()
    ng_steps = get_ng_steps()
    inputs_int = get_inputs_int()

    X = tf.placeholder(tf.float32, [None, tensor_size, num_input])
    Z = tf.placeholder(tf.float32, [None, tensor_size, num_input])
    # Z = tf.placeholder(tf.float32, [None, num_classes])

    # Define weights
    g_weights = {
        # Hidden layer weights => 2*n_hidden because of forward + backward cells
        'out': tf.Variable(tf.random_normal([2*num_hidden, num_classes]))
    }
    g_biases = {
        'out': tf.Variable(tf.random_normal([num_classes]))
    }
    # Define weights
    d_weights = {
        # Hidden layer weights => 2*n_hidden because of forward + backward cells
        'out': tf.Variable(tf.random_normal([2*num_hidden, num_classes]))
    }
    d_biases = {
        'out': tf.Variable(tf.random_normal([num_classes]))
    }

    G_sample = BiRNNGenerator(Z, g_weights, g_biases)  # (Z, hsize=[tensor_size, tensor_size])
    r_logits, r_rep = BiRNNDiscriminator(X, d_weights, d_biases)  # (X, hsize=[tensor_size, tensor_size])
    f_logits, g_rep = BiRNNDiscriminator(X, d_weights, d_biases)  # (G_sample, hsize=[tensor_size, tensor_size], reuse=True)

    disc_loss = tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(logits=r_logits, labels=tf.ones_like(
        r_logits)) + tf.nn.sigmoid_cross_entropy_with_logits(logits=f_logits, labels=tf.zeros_like(f_logits)))
    gen_loss = tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(logits=f_logits, labels=tf.ones_like(f_logits)))

    # gen_vars = tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, scope="GAN/Generator")
    # disc_vars = tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, scope="GAN/Discriminator")

    gen_step = tf.train.RMSPropOptimizer(learning_rate=0.001).minimize(gen_loss, var_list=(g_weights['out'], g_biases['out']))  # G Train step
    disc_step = tf.train.RMSPropOptimizer(learning_rate=0.001).minimize(disc_loss, var_list=(d_weights['out'], d_weights['out']))  # D Train step

    saver = tf.train.Saver()
    sess = tf.Session()
    tf.global_variables_initializer().run(session=sess)

    x_plot = sample_data(n=batch_size, scale=len(inputs_int))

    try:
        for i in range(100001):
            X_batch = sample_data(n=batch_size, scale=len(inputs_int))
            Z_batch = sample_Z(batch_size, tensor_size, get_vocab_len())
            _, dloss = sess.run([disc_step, disc_loss], feed_dict={X: X_batch, Z: Z_batch})
            _, gloss = sess.run([gen_step, gen_loss], feed_dict={Z: Z_batch})

            for _ in range(nd_steps):
                _, dloss = sess.run([disc_step, disc_loss], feed_dict={X: X_batch, Z: Z_batch})
            rrep_dstep, grep_dstep = sess.run([r_rep, g_rep], feed_dict={X: X_batch, Z: Z_batch})

            for _ in range(ng_steps):
                _, gloss = sess.run([gen_step, gen_loss], feed_dict={Z: Z_batch})

            rrep_gstep, grep_gstep = sess.run([r_rep, g_rep], feed_dict={X: X_batch, Z: Z_batch})

            print("Iterations: %d\t Discriminator loss: %.4f\t Generator loss: %.4f" % (i, dloss, gloss))

            if i % 10 == 0:
                save_and_plot()
    except KeyboardInterrupt:
        save_and_plot()
