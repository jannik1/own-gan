#!/usr/bin/env python
# coding: utf-8

# # Train your own text generator using a recurrent neural network!
# by [Mohamed Abdulaziz](https://www.mohamedabdulaziz.com/)
# 
# Train either a bidirectional or normal LSTM recurrent neural network to generate text using any dataset.  **No need to write any code. Just upload your text file and click run!**
# 
# You can generate text after your model has finished training as well! Also you can continue training a pre-trained model if it needs more accuracy.
# 
# **The best part is that all of the training is conducted on a free GPU courtesy of Colaboratory!**
# 
# For more information about the code used in this demo please check this github link.[github link](https://github.com/demmojo/colabrnn)
# 
# 

# ## Before you begin
# 
# Ensure you are running this in Google Chrome. 
# 
# Next, copy the notebook to your Google Drive to keep it as well as save your changes. 
# 
# Also, make sure you are using the GPU runtime type by clicking **Runtime** in the toolbar above and then **Change runtime type**. Check if the hardware accelerator is set to GPU.
# 
# That's it! Run the next code cells!
# 

# In[1]:
# from IPython import get_ipython

# get_ipython().system('git clone https://github.com/demmojo/colabrnn')


# The above code clones the github project on the Colaboratory VM. Next we will install the dependencies and import the necessary packages.

# Note: If you get a **Failed to assign a backend** message that means that no free GPUs are available. You can connect and train using CPUs but that will be much slower. Otherwise, try again later to connect to a server with a GPU. 

# In[1]:


# get_ipython().system('pip install keras==2.2')

# import os


# # What would you like to do?
# 
# If you are training a new model we first need to upload a text file. Then colabrnn will use that to train and generate original text! 
# 
# ## Either train a new model
# 
# Run the cell below and click ***Choose Files*** and select your files from your local computer. (Ideally, your text files should be quite large >1mb).
# 
# Please note that the uploaded file is stored on the Colaboratory VM and** only you** have access to it.
# 
# After uploading the file run the next cell to start the training process! You can see generated text as the training process goes on to see how your model is learning.
# 
# If you prefer to change some parameters please do so before running the cell.
# 
# ## Or continue training a pre-trained model
# 
# Run the cell below and upload the weight, vocabulary and config files as well as the text file.
# 
# After uploading the necessary model files (weight, vocabulary and config files) as well as the text file you can retrain your old model. 
# 
# Change the ***train_new_model*** variable to **False**. Then check whether the file names are correct before running the cell below.
# 
# ## Or generate text with a pre-trained model
# 
# After uploading the necessary model files (weight, vocabulary and config files) as well as the text file skip to the section: **Generate text using your trained model**.
# 
# Check whether the file names are correct before running the cell.

# In[2]:
import numpy
import re
import pandas
import json
from src import my_clustering, data_statistics
from src.clean_text import replace_with_tokens
from src.my_clustering import ubuntu_index, schedule_index, weather_index, navigate_index


def clean_temperature(text):
    text = re.sub("[0-9]+ ?(of)? ?(th)(January|February|March|April|May|June|July|August|September|October|November|Decembr?e?e?r?)?", "<date>", text, flags=re.IGNORECASE)
    text = re.sub("(January|February|March|April|May|June|July|August|September|October|November|Decembr?e?e?r?) ?[0-9]*(th)?", "<date>", text, flags=re.IGNORECASE)
    text = re.sub("(January|February|March|April|May|June|July|August|September|October|November|Decembr?e?e?r?)? ?[0-9]+(th)", "<date>", text, flags=re.IGNORECASE)
    text = re.sub("[0-9]+:?[0-9]* ?(am|pm|m)", "<time>", text, flags=re.IGNORECASE)
    text = re.sub("[0-9]+ ?- ?<temperature>", "<temperature>-<temperature>", text)
    text = re.sub("[0-9]+ (hours|days?)", "<weekly_time>", text)
    text = re.sub("[0-9]+0s ", "<temperature> ", text, flags=re.IGNORECASE)
    text = re.sub("take my medicine", "<event> ", text, flags=re.IGNORECASE)
    text = re.sub("tennis", "<event> ", text, flags=re.IGNORECASE)
    text = re.sub("yoga [class]?", "<event> ", text, flags=re.IGNORECASE)
    text = re.sub("doctor('s)? appointment", "<event> ", text, flags=re.IGNORECASE)
    text = re.sub("swimming", "<event> ", text, flags=re.IGNORECASE)
    text = re.sub("(my son's)? ?football ?(appointments?|game)?", "<event> ", text, flags=re.IGNORECASE)
    return text


first_sentences_only = False
mode = "_navigate"
acceptable_indexes = [navigate_index]
file_path_generated_file = "generated_" + mode + ".txt"
if first_sentences_only:
    file_path_generated_file = "generated_first_sentences" + mode + ".txt"
    # ubuntu_data = pandas.DataFrame(
    #         my_clustering.clean_words((my_clustering.read_from_csv("train-new-converted-min", True))),
    #         columns=["clean_sum", "sum"])
    file_name = "kvret_train_public"
    try:
        with open("../../../data/" + file_name + '.json') as json_file:
            json_data = json.load(json_file)
    except FileNotFoundError:
        with open("../../data/" + file_name + '.json') as json_file:
            json_data = json.load(json_file)
    data_statistics.set_true_labels([ubuntu_index] * 800)
    train_data = data_statistics.clean_words_json(json_data, file_name, True)
    # train_data = pandas.concat([ubuntu_data, train_data], axis=0, ignore_index=True, sort=False)
    true_labels = data_statistics.get_true_labels()
    # generated_data = pandas.DataFrame(4000 * [""], columns=['first_sentence'])
    generated_data = ""
    for i, first_sentence in enumerate(train_data['sum']):
        if acceptable_indexes.__contains__(true_labels[i]):
            if generated_data != "":
                generated_data += "\n" + first_sentence
            else:
                generated_data = first_sentence
            # generated_data.iat[i, 0] = first_sentence
    # generated_data['first_sentence'].replace("", numpy.nan, inplace=True)
    # generated_data.dropna(subset=['first_sentence'], inplace=True)
    # generated_data.to_csv(file_path_generated_file, sep=";")
else:
    # ubuntu_data = pandas.DataFrame(
    #         my_clustering.clean_words((my_clustering.read_from_csv("train-new-converted-min", True))),
    #         columns=["clean_sum", "sum"])
    file_name = "kvret_train_public"
    try:
        with open("../../../data/" + file_name + '.json') as json_file:
            json_data = json.load(json_file)
    except FileNotFoundError:
        with open("../../data/" + file_name + '.json') as json_file:
            json_data = json.load(json_file)
    data_statistics.set_true_labels([ubuntu_index] * 800)
    train_data = data_statistics.clean_words_json(json_data, file_name, False, True)
    # train_data = pandas.concat([ubuntu_data, train_data], axis=0, ignore_index=True, sort=False)
    true_labels = data_statistics.get_true_labels()
    generated_data = ""
    for i, first_sentence in enumerate(train_data['sum']):
        if acceptable_indexes.__contains__(true_labels[i]):
            if generated_data != "":
                generated_data += "\n" + first_sentence
            else:
                generated_data = first_sentence
generated_data = replace_with_tokens(generated_data)
generated_data = clean_temperature(generated_data)
with open(file_path_generated_file, 'w') as f:
    f.write(generated_data)
# uploaded = files.upload()
# all_files = [(name, os.path.getmtime(name)) for name in os.listdir()]
# latest_uploaded_file = sorted(all_files, key=lambda x: -x[1])[0][0]


# In[6]:


from colabrnn.rnn import CharGen
from colabrnn.rnn.train_model import train

train_new_model = True

if train_new_model:  # Create a new neural network model and train it
    char_gen = CharGen(name=mode[1:],
                       bidirectional=True,  # Boolean. Train using a bidirectional LSTM or unidirectional LSTM. See this coursera video for more information: https://www.coursera.org/lecture/nlp-sequence-models/bidirectional-rnn-fyXnn
                       rnn_size=128,  # Number of neurons in each layer of your neural network (default 128)
                       rnn_layers=3,  # Number of layers in your neural network (default 3)       
                       embedding_dims=75,  # Size of the embedding layer (default 75)
                       input_length=180  # Number of characters considered for prediction (default 60)
                      )
    train(text_filepath=file_path_generated_file,
          chargen=char_gen,
          gen_text_length=500,  # Number of characters to be generated. Average number of characters in a word is approximately 5. (default 500)
          num_epochs=100,  # One epoch is when an entire dataset is passed forward and backward through the neural network only once (default 10)
          batch_size=512,  # Total number of training examples present in a single batch. More is faster but there are memory constraints. If you are experiencing insufficient memory issues reduce this number. (default 512)
          train_new_model=train_new_model
         )  

    print(char_gen.model.summary())
else:  # Continue training an old model
    text_filename = file_path_generated_file  # specify correct filename if you are retraining an old model
    char_gen = CharGen(name=model_name,
                      weights_filepath='colab_weights.hdf5',  # specify correct filename if you are retraining an old model
                      vocab_filepath='colab_vocabulary.json',  # specify correct filename if you are retraining an old model
                      config_filepath='colab_config.json')  # specify correct filename if you are retraining an old model
    
    train(text_filename, char_gen, train_new_model=train_new_model, num_epochs=10)  # change num_epochs to specify number of epochs to continue training


# ## Save the model files
# 
# Run the cell below to save the model files locally. You can upload them again later to retrain.

# In[7]:

# TODO: Fix saving
# files.download('{}_weights.hdf5'.format(model_name))
# files.download('{}_vocabulary.json'.format(model_name))
# files.download('{}_config.json'.format(model_name))


# ## Generate text using your trained model!
# 
# Run the cell below to generate samples of your trained model. 
# 
# You can specify the starting text  for the model by changing the ***prefix***  variable to use as the beginning of the generated text.
# 
# You can also define how long you want your generated text to be by ***change gen_text_length*** variable.
# 
# Have fun!

# In[6]:


from colabrnn.rnn import CharGen

char_gen = CharGen(weights_filepath='colab_weights.hdf5',  # specify correct filename 
                   vocab_filepath='colab_vocabulary.json',  # specify correct filename
                   config_filepath='colab_config.json')  # specify correct filename 

char_gen.generate(gen_text_length=500, prefix='To be or not to be,')


# If at any time you would like to list the contents of the current directory use the following command:

# In[ ]:


# get_ipython().system('ls')


# If you would like to restart or reset the Colaboratory VM you can run the following cell:

# In[ ]:


# get_ipython().system('kill -9 -1')


# ### If you have any questions, suggestions or would like to share your project, please contact me [here](https://www.mohamedabdulaziz.com/#contact).
# 
# ### You can also check out my other projects on [Github](https://github.com/demmojo).
