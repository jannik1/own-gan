import re
import numpy as np
import pandas
import matplotlib
import string
import nltk
import platform
import datetime
import random
import src.cluster_similarity
import os
import csv
import fasttext
import json
import pickle
import warnings
import spacy

from src import clean_text

spacy_nlp = spacy.load('en')

from sklearn.linear_model import LogisticRegression
from sklearn.mixture import GaussianMixture
from sklearn.pipeline import Pipeline, FeatureUnion

warnings.filterwarnings(action='ignore', category=UserWarning, module='gensim')

from sklearn.cluster import KMeans, MeanShift
from sklearn.decomposition import PCA, TruncatedSVD
from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer
from sklearn import metrics
from sklearn.feature_extraction import stop_words
from nltk.stem.snowball import SnowballStemmer
from nltk.stem import WordNetLemmatizer
from nltk.tokenize import word_tokenize
from multiprocessing import Process, Manager, Value
from gensim.utils import simple_preprocess
from gensim.models.doc2vec import TaggedDocument, Doc2Vec
from six.moves import input
from nltk.text import TextCollection
from wikipedia2vec import Wikipedia2Vec

n_jobs = 4
max_df_fasttext = 0.01
min_df_fasttext = 0.001
LOG = False
now = datetime.datetime.now()
word_to_vector = dict()
read_data = False
loaded_glove = False
ubuntu_index = 2
weather_index = 1
schedule_index = 0
navigate_index = 3
glove_dimensions = 50
fasttext_dimensions = 50
wikipedia2vec_dimensions = 100
glove_trained_data_file_name = "trained data/glove-vectors-text8.pkl"
glove_trained_data_dict_file_name = "trained data/glove/glove.6B.50d-dict.pkl"
fasttext_trained_data_file_name = "trained data/fasttext/wiki.en.bin"
wikipedia2vec_trained_data_file_name = "trained data/enwiki_20180420_100d.pkl"
if __name__ == '__main__':
    if platform.node() != 'HP-Laptop':
        print('no display found. Using non-interactive Agg backend')
        matplotlib.use('Agg')
    else:
        matplotlib.use('TkAgg')
    nltk.download('wordnet')
    nltk.download('punkt')
    SHOW = False
    SCRIPTED = False
    CREATE_TSNE = True
    SAMPLE_SIZE = 10000
    clustering_alg_name = "meanshift"
    to_evaluate = True
    to_fill_textcollection = True
    min_number_of_clusters = 1
    max_number_of_clusters = 10
    number_of_clusters = 0
    vectorization_mode = "tf-idf"
    step_size = 1
    dir_name = ""
    reduced_counts_plus_centers = []
    my_dialogs = TextCollection([])
    true_labels = []
    global_stop_words = ["eot", "eou"]
    # global_stop_words = ["eot", "eou", "use", "tri", "try", "just", "doe", "dont", "did", "does", "like", "hi", "yes",
    #                      "no", "ubuntu", "help", "way", "run", "instal", "work", "know", "need", "want", "stuff",
    #                      "thank", "look", "make", "think", "mean", "ok", "problem", "thing", "someth", "set", "say",
    #                      "sure", "ask", "right", "hello", "file", "window", "install", "installed", "1204",
    #                      "using", "don"]
    for stop_word in stop_words.ENGLISH_STOP_WORDS:
        global_stop_words.append(stop_word)
    manager = Manager()
    nearest_dialogs = []


def get_time():
    return now.strftime("%d-%m-%Y %H-%M")


def read_from_csv(csv_file_name, only_first_utterance=False, only_rows=[]):
    global true_labels
    nrows = 100
    if csv_file_name == "kvret_train_public":
        nrows = 800
        csv_file_name = "train-new-converted"
    elif csv_file_name == "kvret_test_public":
        csv_file_name = "test-new-converted"
    elif csv_file_name == "kvret_dev_public":
        csv_file_name = "valid-new-converted"
    if csv_file_name == "train-full-converted" or csv_file_name == "train-new-converted"\
            or csv_file_name == "train-converted" or csv_file_name == "train-new-converted-min":
        label_name = "Utterance"
        nrows = 800
    else:
        label_name = "Ground Truth Utterance"
    true_labels = [ubuntu_index] * nrows
    try:
        complete_data = pandas.read_csv("../../data/unlemmatized/" + csv_file_name + ".csv", encoding="ISO-8859-1", sep=';',
                                        nrows=15000)
    except FileNotFoundError:
        complete_data = pandas.read_csv("../../../data/unlemmatized/" + csv_file_name + ".csv", encoding="ISO-8859-1", sep=';',
                                        nrows=15000)
    complete_data = complete_data.fillna(' ')
    # for index, row in enumerate(complete_data["Context"]):
    #     if complete_data["Label"][index] == 1:
    #         complete_data.set_value("Context", index, complete_data.loc[index]["Context"]
    #                                 + complete_data.loc[index][label_name])
    data_subset = pandas.DataFrame(columns=['sum', 'clean_sum', 'Full_Context', 'cluster_num', 'distance', 'x', 'y', ])
    dialog_number = 0
    while len(data_subset) < nrows:
        if dialog_number >= len(complete_data):
            print("Stopped clustering because file contained not enough utterances of 'ubuntu'")
            exit()
        tokenized_dialog = word_tokenize(complete_data["Context"][dialog_number])
        tokenized_utterance = word_tokenize(complete_data[label_name][dialog_number])
        tokenized_dialog.extend(tokenized_utterance)
        turns = []
        for turn in [x.strip() for x in complete_data["Context"][dialog_number].split("__eot__")]:
            if turn != "":
                turns.append(turn)
        no_of_turns = len(turns) + 1
        if csv_file_name == "train-new-converted-min" or csv_file_name == "test-new-converted-min":
            no_of_turns = 2
        if (8 < len(tokenized_dialog) <= 120 and\
                no_of_turns % 2 == 0 and\
                (tokenized_dialog.__contains__("nvidia")
                 or tokenized_dialog.__contains__("ati")
                 or tokenized_dialog.__contains__("graphics")
                 or tokenized_dialog.__contains__("network")
                 or tokenized_dialog.__contains__("ubuntu")))\
                or csv_file_name == "train-new-converted-min" or csv_file_name == "test-new-converted-min":
            complete_data.set_value(dialog_number, "Context", complete_data.loc[dialog_number]["Context"]
                                    + complete_data.loc[dialog_number][label_name])
            data_subset = data_subset.append(complete_data.loc[dialog_number], ignore_index=True)
        dialog_number += 1
    if only_first_utterance:
        for dialog_index, dialog in enumerate(data_subset['Context']):
            eot_index = dialog.find("__eot__")
            first_sentence = re.sub("__eou__", "", dialog[0:eot_index])
            # first_sentence = re.search(r'([^_]*)__eot__', dialog).group(1)
            data_subset.set_value(dialog_index, "Full_Context", dialog)
            data_subset.set_value(dialog_index, "Context", first_sentence)
            print("FIRST SENTENCE: " + first_sentence)
    if len(only_rows) > 0:
        data_subset = data_subset.take(only_rows)
    return data_subset


def clean_words(data):
    data = data.fillna('NULL')
    data["sum"] = data["Context"]
    # lambda dialog: stem_sentence(re.sub('[\t,.:;_=+*<>~^\-"!?@%&#$`/|\'()[\]{}]', ' ',
    #                                     remove_apostrophes_typos(re.sub('__eou__', '',
    #                                                                     re.sub('__eot__', '', dialog.lower()))))))
    data['clean_sum'] = data["Context"].apply(lambda sentence: clean_sentence(sentence))
    return data


def clean_words_json(data, only_first_sentence=False, only_rows=[]):
    global true_labels
    returned_data = pandas.DataFrame(columns=['sum', 'clean_sum', 'cluster_num', 'distance', 'x', 'y', 'index_in_dialogs', ],
                                     index=[i for i in range(len(data))])
    returned_data = returned_data.fillna('NULL')
    if len(only_rows) > 0:
        for i, row in enumerate(only_rows):
            only_rows[i] = row - 800
        for i, dialog_index in enumerate(only_rows):
            if data[dialog_index]['scenario']['task']['intent'] == "weather":
                if file_name == "kvret_train_public":
                    true_labels.append(weather_index)
                else:
                    true_labels.append(weather_index)
            elif data[dialog_index]['scenario']['task']['intent'] == "navigate":
                if file_name == "kvret_train_public":
                    true_labels.append(navigate_index)
                else:
                    true_labels.append(navigate_index)
            elif data[dialog_index]['scenario']['task']['intent'] == "schedule":
                if file_name == "kvret_train_public":
                    true_labels.append(schedule_index)
                else:
                    true_labels.append(schedule_index)
            for utterance_index, utterance in enumerate(data[dialog_index]['dialogue']):
                if not only_first_sentence or utterance_index == 0:
                    if str(returned_data['clean_sum'][i]) != "NULL":
                        returned_data.loc[i]['sum'] = str(returned_data['sum'][i]) + " __eot__ "\
                                                                 + utterance['data']['utterance']
                        returned_data.loc[i]['clean_sum'] = str(returned_data['clean_sum'][i]) + " "\
                            + clean_sentence(utterance['data']['utterance'])
                        returned_data.loc[i]['index_in_dialogs'] = dialog_index
                    else:
                        returned_data.loc[i]['clean_sum'] = clean_sentence(utterance['data']['utterance'])
                        returned_data.loc[i]['sum'] = utterance['data']['utterance']
                        returned_data.loc[i]['index_in_dialogs'] = dialog_index
    else:
        for dialog_index, dialog in enumerate(data):
            if dialog['scenario']['task']['intent'] == "weather":
                if file_name == "kvret_train_public":
                    true_labels.append(weather_index)
                else:
                    true_labels.append(weather_index)
            elif dialog['scenario']['task']['intent'] == "navigate":
                if file_name == "kvret_train_public":
                    true_labels.append(navigate_index)
                else:
                    true_labels.append(navigate_index)
            elif dialog['scenario']['task']['intent'] == "schedule":
                if file_name == "kvret_train_public":
                    true_labels.append(schedule_index)
                else:
                    true_labels.append(schedule_index)
            for utterance_index, utterance in enumerate(dialog['dialogue']):
                if not only_first_sentence or utterance_index == 0:
                    if str(returned_data['clean_sum'][dialog_index]) != "NULL":
                        returned_data.loc[dialog_index]['sum'] = str(returned_data['sum'][dialog_index]) + " __eot__ "\
                                                                 + utterance['data']['utterance']
                        returned_data.loc[dialog_index]['clean_sum'] = str(returned_data['clean_sum'][dialog_index]) + " "\
                            + clean_sentence(utterance['data']['utterance'])
                        returned_data.loc[dialog_index]['index_in_dialogs'] = dialog_index
                    else:
                        returned_data.loc[dialog_index]['clean_sum'] = clean_sentence(utterance['data']['utterance'])
                        returned_data.loc[dialog_index]['sum'] = utterance['data']['utterance']
                        returned_data.loc[dialog_index]['index_in_dialogs'] = dialog_index
    return returned_data


def clean_sentence(sentence):
    # old_sentence = sentence
    sentence = sentence.lower()
    sentence = remove_apostrophes_typos(sentence)
    # sentence = re.sub(r'[0123456789]*', '', sentence)
    # # sentence = remove_www(sentence)
    sentence = remove_special_chars(sentence)
    # sentence = stem_sentence(sentence)
    sentence_tokenized = word_tokenize(sentence)
    filter(lambda x: x not in global_stop_words, sentence_tokenized)
    sentence = "".join([" " + i if not i.startswith("'") and i not in string.punctuation else i
                        for i in sentence_tokenized]).strip()
    sentence = clean_text.replace_with_tokens(sentence)
    # if LOG and sentence_tokenized.__contains__("im"):
    #     print("SHEEESH")
    # if LOG and len(sentence_tokenized) < 3:
    #     print("BEFORE: " + old_sentence + "\n AFTER: " + sentence + "\n")
    return sentence


def remove_apostrophes_typos(sentence):
    list_of_abbrvs = [("can", "t", "cannot"), ("didn", "t", "did not"), ("doesn", "t", "does not"),
                      ("isn", "t", "is not"), ("won", "t", "will not"), ("wouldn", "t", "would not"),
                      ("i", "ll", "i will"), ("you", "ll", "you will"), ("i", "m", "i am"), ("i", "ve", "i have"),
                      ("i", "d", "i would"), ("you", "d", "you would"), ("aren", "t", "are not"),
                      ("you", "re", "you are"), ("he", "s", "he is"), ("she", "s", "she is"), ("it", "s", "it is"),
                      ("we", "re", "we are"), ("they", "re", "they are"), ("that", "s", "that is"),
                      ("there", "s", "there is"), ("what", "s", "what is"), ("where", "s", "where is"),
                      ("why", "s", "why is"), ("who", "s", "who is")]
    for (prefix, suffix, repl) in list_of_abbrvs:
        sentence = re.sub(r'\b' + prefix + r'[@\-.,`"\';#? ]+' + suffix + r'\b', repl, sentence)
    sentence = sentence.replace('anyon', 'anyone')
    sentence = sentence.replace('anyonee', 'anyone')
    sentence = sentence.replace('oubuntu', 'ubuntu')
    sentence = sentence.replace('ubunut', 'ubuntu')
    return sentence


def remove_www(sentence):
    sentence = re.sub(r'http\S+', '', sentence)
    sentence = re.sub(r'www.\S+', '', sentence)
    sentence = re.sub(r'\S+\.\S+\.(com|org|de)\S+', '', sentence)
    return sentence


def remove_special_chars(sentence):
    sentence = re.sub('__eou__', '', sentence)
    sentence = re.sub('__eot__', '', sentence)
    sentence = re.sub('[\t,.:;_=+*~^\-"!?@%&#$`/|\'()[\]{}]', ' ', sentence)
    return sentence


def stem_sentence(sentence):
    lemmatizer = WordNetLemmatizer()
    sentence = lemmatizer.lemmatize(sentence, "v")
    snowball_stemmer = SnowballStemmer("english")
    sentence = snowball_stemmer.stem(sentence)
    return sentence


def read_corpus(cleaned_words, tokens_only=False):
    for i, line in enumerate(cleaned_words):
        if tokens_only:
            yield simple_preprocess(line)
        else:
            # For training data, add tags
            yield TaggedDocument(simple_preprocess(line), [i])


def start_doc2vec(cleaned_words):
    global model
    dialog_vectors = []
    train_corpus = list(read_corpus(cleaned_words))
    # test_corpus = list(read_corpus(cleaned_words, tokens_only=True))
    model = Doc2Vec(vector_size=200, min_count=10, epochs=20)
    model.build_vocab(train_corpus)
    model.train(train_corpus, total_examples=model.corpus_count, epochs=model.epochs)
    for line in cleaned_words:
        dialog_vectors.append(model.infer_vector(word_tokenize(line)))
    model_file_name = dir_name + file_name + " - doc2vec model"
    model.save(model_file_name)
    model = Doc2Vec.load(model_file_name)
    return dialog_vectors
    # ranks = []
    # second_ranks = []
    # for doc_id in range(len(train_corpus)):
    #     inferred_vector = model.infer_vector(train_corpus[doc_id].words)
    #     sims = model.docvecs.most_similar([inferred_vector], topn=len(model.docvecs))
    #     rank = [docid for docid, sim in sims].index(doc_id)
    #     ranks.append(rank)
    #
    #     second_ranks.append(sims[1])
    # collections.Counter(ranks)  # Results vary between runs due to random seeding and very small corpus


# Source: https://jonasteuwen.github.io/np/python/multiprocessing/2017/01/07/multiprocessing-np-array.html
def fill_word_to_vector_per_window(data, vector_dimensions, word_to_vector, job_id):
    vectors_to_read = len(data['word'])
    for index in range(job_id, vectors_to_read, n_jobs):
        word_to_vector[data["word"][index]] = [data[str(i)][index] for i in (range(vector_dimensions))]
        if (index / n_jobs) % 1000 == 0:
            print("Thread " + str(os.getpid()) + " read " + str(index / n_jobs) + " vectors out of "
                   + str(vectors_to_read / n_jobs) + ".")
    print("Thread " + str(os.getpid()) + " finished reading " + str(vectors_to_read / n_jobs) + " vectors.")


def get_vectors(vector_file_name, vector_dimensions, skip_rows, dict_available=False):
    word_to_vector = manager.dict()
    if not dict_available:
        data_columns = [str(i) for i in range(vector_dimensions)]
        data_columns.insert(0, "word")
        dtype = {}
        for col in data_columns:
            dtype[col] = float
        dtype['word'] = str
        with open(vector_file_name, 'rb') as pickle_file:
            data = pickle.load(pickle_file, encoding="ISO-8859-1")
        # data = pandas.read_csv(vector_file_name, sep=" ", header=None, encoding="ISO-8859-1", quoting=csv.QUOTE_NONE,
        #                        low_memory=False, dtype=dtype, skiprows=skip_rows)
        if vector_dimensions == 100:
            data = data.drop(columns=101)
        data.columns = data_columns

        start_multiple_threads(target=fill_word_to_vector_per_window, args=(data, vector_dimensions, word_to_vector))
    else:
        with open(glove_trained_data_dict_file_name, 'rb') as pickle_file:
            word_to_vector = pickle.load(pickle_file, encoding="ISO-8859-1")
    return word_to_vector


def compute_vectors_per_window(word_to_vector, cleaned_words, vector_dimensions, dialog_vectors, log_threadsafe,
                               matches, unks, local_fasttext_model, vector_mode, my_local_dialogs, job_id):
    words_to_vectorize = len(cleaned_words)
    for index in range(job_id, words_to_vectorize, n_jobs):
        current_vector = [0] * vector_dimensions
        tokenized_words = word_tokenize(cleaned_words[index])
        for word in tokenized_words:
            if vector_mode == "glove":
                if word_to_vector.__contains__(word):
                    matches.value += 1
                    current_vector = np.sum([current_vector,
                                             np.multiply(word_to_vector[word], my_local_dialogs.idf(word))
                                             ], axis=0)
                elif log_threadsafe:
                    print("word " + word + " could not be found.")
                    current_vector = np.sum([current_vector,
                                             np.multiply(word_to_vector["<unk>"], my_local_dialogs.idf(word))
                                             ], axis=0)
                    unks.value += 1
                else:
                    current_vector = np.sum([current_vector,
                                             np.multiply(word_to_vector["<unk>"], my_local_dialogs.idf(word))
                                             ], axis=0)
                    unks.value += 1
            elif vector_mode == "fasttext":
                word_vector_to_add = local_fasttext_model.get_word_vector(word)
                if to_fill_textcollection:
                    # if np.log(1 / min_df_fasttext) > my_local_dialogs.idf(word) > np.log(1 / max_df_fasttext):
                        word_vector_to_add = word_vector_to_add
                        matches.value += 1
                    # else:
                    #     unks.value += 1
                    #     word_vector_to_add = [0] * vector_dimensions
                current_vector = np.sum([current_vector, word_vector_to_add], axis=0)
        dialog_vectors.append(current_vector)
        if (index / n_jobs) % 1000 == 0:
            print("Thread " + str(os.getpid()) + " computed vectors for " + str(index / n_jobs) + " words out of "
                   + str(words_to_vectorize / n_jobs) + ".")
    print("Thread " + str(os.getpid()) + " finished computing " + str(words_to_vectorize / n_jobs)
           + " word vectors.")


def start_multiple_threads(target, args):
    processes = []
    for i in range(0, n_jobs):
        process = Process(target=target, args=args+(i,))
        process.start()
        processes.append(process)
    for p in processes:
        p.join()


def start_glove(cleaned_words, dict_available=False, local_my_dialogs=None):
    global manager
    global word_to_vector
    global loaded_glove
    global my_dialogs
    manager = Manager()
    cleaned_words = [dialog for dialog in cleaned_words]
    if local_my_dialogs != None:
        my_dialogs = local_my_dialogs
    if not loaded_glove:
        word_to_vector = get_vectors(glove_trained_data_dict_file_name, glove_dimensions, [], dict_available)
    loaded_glove = True
    dialog_vectors = manager.list()
    matches = Value('i', 0)
    unks = Value('i', 0)
    start_multiple_threads(target=compute_vectors_per_window, args=(word_to_vector, cleaned_words, glove_dimensions,
                                                                    dialog_vectors, LOG, matches, unks,
                                                                    None, "glove", my_dialogs))
    print("Found %d words and did not find %d words in vocabulary" % (matches.value, unks.value))
    print("Out-Of-Vocabulary rate: %d percent" % ((unks.value / (unks.value + matches.value)) * 100))
    return dialog_vectors


def start_fasttext(cleaned_words):
    global fasttext_model
    word_to_vector = []
    # word_to_vector = get_vectors(fasttext_trained_data_file_name, fasttext_dimensions, [0])
    fasttext_model = fasttext.load_model(fasttext_trained_data_file_name)
    dialog_vectors = manager.list()
    matches = Value('i', 0)
    unks = Value('i', 0)
    start_multiple_threads(target=compute_vectors_per_window, args=(word_to_vector, cleaned_words, fasttext_dimensions,
                                                                    dialog_vectors, LOG, matches, unks, fasttext_model,
                                                                    "fasttext", my_dialogs))
    print("Found %d words and did not find %d words in vocabulary" % (matches.value, unks.value))
    print("Out-Of-Vocabulary rate: %d percent" % ((unks.value / (unks.value + matches.value)) * 100))
    return dialog_vectors


def start_wikipedia2vec(cleaned_words):
    global wiki2vec
    dialog_vectors = []
    matches = 0
    unks = 0
    wiki2vec = Wikipedia2Vec.load(wikipedia2vec_trained_data_file_name)
    for line in cleaned_words:
        dialog_vector = [0] * wikipedia2vec_dimensions
        for word in word_tokenize(line):
            try:
                word_vector = wiki2vec.get_word_vector(word)
                matches += 1
            except KeyError:
                word_vector = [0] * wikipedia2vec_dimensions
                unks += 1
            dialog_vector = np.sum([dialog_vector, word_vector], axis=0)
        dialog_vectors.append(dialog_vector)
    print("Found %d words and did not find %d words in vocabulary" % (matches, unks))
    print("Out-Of-Vocabulary rate: %d percent" % ((unks / (unks + matches)) * 100))
    return dialog_vectors


def start_spacy(cleaned_words):
    dialog_vectors = []
    for dialog in cleaned_words:
        analysed_dialog = spacy_nlp(dialog)
        dialog_vectors.append(analysed_dialog.vector)
    return dialog_vectors


def vectorize_words(mode, to_return=False, local_csv_data=pandas.DataFrame(), loaded_vectorizer=TfidfVectorizer()):
    global vectorizer
    global counts
    global csv_data
    global file_name
    global read_data
    global csv_train_data
    max_df = 1.0
    min_df = 5
    if to_return:
        csv_data = local_csv_data
        if not read_data:
            vectorizer = loaded_vectorizer
    if 10 <= len(csv_data) < 2100:
        max_df = 0.8  # 0.10
        min_df = 3
        print("Small dataset detected, adjusting max_df and min_df")
    if len(csv_data) < 10:
        max_df = 0.5  # 0.10
        min_df = 1
        print("Very small dataset detected, adjusting max_df and min_df")
    if len(csv_data) > 30000:
        max_df = 0.05
        min_df = 10
        print("Big dataset detected, adjusting max_df and min_df")
    # if to_return:
    #     vectorizer.set_params(union__word_vec__min_df=min_df, union__word_vec__max_df=max_df,
    #                           union__char_vec__min_df=min_df, union__char_vec__max_df=max_df)
    if mode == "tf-idf":
        if to_return:
            file_name = "kvret_train_public"
            if not read_data:
                read_ubuntu = read_from_csv("train-new-converted-min", only_first_utterance=True)
                ubuntu_train_data = pandas.DataFrame(clean_words(read_ubuntu), columns=["clean_sum"])
                ubuntu_train_data = ubuntu_train_data.assign(sum=read_ubuntu['Context'])
                read_data = True
                with open("../../data/" + file_name + '.json') as json_train_file:
                    json_train_data = json.load(json_train_file)
                csv_train_data = clean_words_json(json_train_data, only_first_sentence=True)
                csv_train_data = pandas.concat([ubuntu_train_data, csv_train_data], axis=0, ignore_index=True, sort=False)
                temp_csv_train_data = pandas.DataFrame([csv_train_data['clean_sum'][index] for index, _ in enumerate(csv_train_data['clean_sum'])], columns=["clean_sum"])
                csv_train_data = temp_csv_train_data.assign(sum=[csv_train_data['sum'][index] for index, _ in enumerate(csv_train_data['sum'])])
            csv_local_train_data = pandas.concat([csv_train_data, csv_data], axis=0, ignore_index=True, sort=False)
            if not read_data:
                vectorizer = vectorizer.fit(csv_local_train_data['clean_sum'])
            counts = vectorizer.transform(csv_local_train_data['clean_sum'])
        else:
            vectorizer = Pipeline(steps=[
                        ('union', FeatureUnion([
                            ('word_vec', TfidfVectorizer(analyzer='word', decode_error='strict', binary=True,
                                                         max_df=max_df, min_df=min_df, dtype=np.float32,
                                                         ngram_range=(1, 2), sublinear_tf=True)),
                            ('char_vec', TfidfVectorizer(analyzer='char', decode_error='strict', binary=True,
                                                         max_df=max_df, min_df=min_df, dtype=np.float32,
                                                         ngram_range=(3, 8), sublinear_tf=True))
                            ])),
                        #('lreg', LogisticRegression())
                        ])
            # vectorizer = TfidfVectorizer(analyzer='word', decode_error='strict', binary=True,
            #                              max_df=max_df, min_df=min_df, dtype=np.float32, ngram_range=(1, 2)) # , ngram_range=(4, 15))  # , max_features=200)
            vectorizer = vectorizer.fit(csv_data['clean_sum'], csv_data['clean_sum'])
            counts = vectorizer.transform(csv_data['clean_sum'])
    elif mode == "count":
        vectorizer = CountVectorizer(analyzer='word', decode_error='strict', binary=True,
                                     max_df=max_df, min_df=min_df, dtype=np.float32) # , ngram_range=(4, 15))  # , max_features=200)
        counts = vectorizer.fit_transform(csv_data['clean_sum'])
    elif mode == "doc2vec":
        counts = start_doc2vec(csv_data['clean_sum'])
    elif mode == "glove":
        counts = start_spacy(csv_data['clean_sum'])
    elif mode == "fasttext":
        counts = start_fasttext(csv_data['clean_sum'])
    elif mode == "wikipedia2vec":
        counts = start_wikipedia2vec(csv_data['clean_sum'])
    if to_return:
        return counts


def reduce_counts():
    global reduced_counts
    global pca
    reduced_size = 2
    print("Reducing counts to " + str(reduced_size) + " dimensions")
    if vectorization_mode == "doc2vec" or vectorization_mode == "glove" or vectorization_mode == "fasttext"\
            or vectorization_mode == "wikipedia2vec":
        pca = PCA(n_components=reduced_size)
        pca = pca.fit(counts)
        reduced_counts = pca.transform(counts)
    else:
        try:
            pca = PCA(n_components=reduced_size)
            pca = pca.fit(counts.todense())
            reduced_counts = pca.transform(counts.todense())
        except (MemoryError, ValueError):
            print("Using TruncatedSVD because of Memory Error when converting counts to dense array")
            pca = TruncatedSVD(n_components=reduced_size)
            pca = pca.fit(counts.todense())
            reduced_counts = pca.transform(counts)
    print("Finished reducing!")


def reduce_counts_plus_centers():
    global reduced_counts_plus_centers
    global pca
    # global counts_plus_centers
    reduced_size = 2
    if counts[0].size < reduced_size:
        reduced_size = counts[0].size - 1
    if reduced_size < 2:
        reduced_size = 2
    print("Reducing counts plus centers to " + str(reduced_size) + " dimensions")
    if clustering_alg_name == "gaussian":
        reduced_counts_plus_centers = np.append(reduced_counts, clustering_alg.means_, axis=0)
    else:
        reduced_counts_plus_centers = np.append(reduced_counts, clustering_alg.cluster_centers_, axis=0)
    try:
        pca = PCA(n_components=reduced_size)
        pca = pca.fit(reduced_counts_plus_centers)
        reduced_counts_plus_centers = pca.transform(reduced_counts_plus_centers)
    except (MemoryError, ValueError):
        print("Using TruncatedSVD because of Memory Error when converting counts to dense array")
        pca = TruncatedSVD(n_components=reduced_size)
        pca = pca.fit(reduced_counts_plus_centers)
        reduced_counts_plus_centers = pca.transform(reduced_counts_plus_centers)
    # counts_plus_centers = np.append(counts.toarray(), clustering_alg_not_reduced.cluster_centers_, axis=0)
    print("Finished reducing!")


def write_source_to_csv(source, csv_file_name, dir_name):
    source.to_csv(dir_name + csv_file_name + " - " + get_time() + '.csv', index=False,
                  encoding="ISO-8859-1", sep=';', decimal=',')


def set_cluster_nums(cluster_nums, use_gaussian=True):
    global csv_data
    global nearest_dialogs_cluster_nums
    if not use_gaussian:
    #     nearest_dialogs_cluster_nums = [cluster_nums[index] for index in nearest_dialogs]
        nearest_dialogs_cluster_nums = cluster_nums[0:4]  # [csv_data['cluster_num'][i] for i in range(4)]
    for i, num in enumerate(cluster_nums):
        if use_gaussian or i > 3:
            csv_data.set_value(i, 'cluster_num', num)


def set_nearest_dialogs(local_dialogs):
    global nearest_dialogs
    nearest_dialogs = local_dialogs


def get_nearest_dialog(i):
    global correct_ubuntu_index
    global correct_schedule_index
    global correct_navigate_index
    global correct_weather_index
    global ubuntu_index
    global schedule_index
    global navigate_index
    global weather_index
    if len(nearest_dialogs) < 1:
        min_distance = 3000000
        nearest_dialog_index = -1
        for index in range(len(csv_data)):
            if csv_data['cluster_num'][index] == i:
                distance = calc_distance(index)
                if distance < min_distance:
                    min_distance = distance
                    nearest_dialog_index = index
        return nearest_dialog_index
    else:
        if i == 0:
            correct_ubuntu_index = ubuntu_index
            ubuntu_index = nearest_dialogs_cluster_nums[i]
            print("UBUNTU: " + str(ubuntu_index))
        elif i == 1:
            correct_schedule_index = schedule_index
            schedule_index = nearest_dialogs_cluster_nums[i]  # nearest_dialogs[i]
            print("SCHEDULE: " + str(schedule_index))
        elif i == 2:
            correct_weather_index = weather_index
            weather_index = nearest_dialogs_cluster_nums[i]
            print("WEATHER: " + str(weather_index))
        elif i == 3:
            correct_navigate_index = navigate_index
            navigate_index = nearest_dialogs_cluster_nums[i]
            print("NAVIGATE: " + str(navigate_index))
        return -1
        # for index, dialog_number in enumerate(nearest_dialogs):
        #    if csv_data['cluster_num'][dialog_number] == i:
        #         return nearest_dialogs[index]
    # return -1


def write_clusters_to_csv(csv_file_name):
    clusters = []
    if clustering_alg_name == "gaussian":
        cluster_centers = clustering_alg.means_
    else:
        cluster_centers = clustering_alg.cluster_centers_
    for i in range(np.shape(cluster_centers)[0]):
        index_of_nearest_dialog = get_nearest_dialog(i)
        if vectorization_mode == "tf-idf":
            data_cluster = pandas.concat([pandas.Series(
                vectorizer.steps[0][1].transformer_list[0][1].get_feature_names()), pandas.DataFrame(
                    counts[index_of_nearest_dialog].toarray().transpose())], axis=1)
        elif vectorization_mode == "count":
            data_cluster = pandas.concat([pandas.Series(vectorizer.get_feature_names()), pandas.DataFrame(
                    counts[index_of_nearest_dialog].toarray().transpose())], axis=1)
        elif vectorization_mode == "doc2vec":
            # TODO: Add keywords
            data_cluster = pandas.concat(
                [pandas.Series(model.docvecs.offset2doctag), pandas.DataFrame(cluster_centers[i])],
                axis=1)
        elif vectorization_mode == "fasttext" or vectorization_mode == "wikipedia2vec":
            features = pandas.Series()
            influences = pandas.DataFrame()
            for word in word_tokenize(csv_data['clean_sum'][index_of_nearest_dialog]):
                features = features.append(pandas.Series([word]))
                if vectorization_mode == "wikipedia2vec" \
                        or np.log(1 / min_df_fasttext) > my_dialogs.idf(word) > np.log(1 / max_df_fasttext):
                    if vectorization_mode == "fasttext":
                        word_vector = fasttext_model.get_word_vector(word)
                    elif vectorization_mode == "wikipedia2vec":
                        try:
                            word_vector = wiki2vec.get_word_vector(word)
                        except KeyError:
                            word_vector = [0] * wikipedia2vec_dimensions
                    if np.count_nonzero(word_vector) == len(word_vector):
                        influence = np.divide(counts[index_of_nearest_dialog], word_vector)
                    else:
                        influence = np.array([0] * len(counts[0]))
                else:
                    influence = np.array([0] * len(counts[0]))
                influence = np.sqrt(influence.dot(influence))
                influences = influences.append(pandas.DataFrame([influence]))
            data_cluster = pandas.concat([features, influences], axis=1)
        else:
            # TODO: Add keywords
            data_cluster = pandas.concat(
                [pandas.Series(), pandas.DataFrame(cluster_centers[i])],
                axis=1)
        data_cluster.columns = ['keywords', 'weights']
        data_cluster = data_cluster.sort_values(by=['weights'], ascending=False)
        data_clust = data_cluster.head(n=10)['keywords'].tolist()
        data_clust.append(cluster_centers[i])
        # data_clust.append(clustering_alg_not_reduced.cluster_centers_[i])
        data_clust.append("Nearest neighbor index: " + str(index_of_nearest_dialog))
        if to_evaluate:
            data_clust.append(["Silhouette: " + str(silhouette_score),
                              "Calinski-Harabaz: " + str(calinski_harabaz_score),
                               "Mean Squared Error: " + str(mean_squared_error),
                               "Intra Cluster Distance: " + str(intra_cluster_dist),
                               "Inter Cluster Distance: " + str(inter_cluster_dist),
                               "Accuracy: " + str(accuracy_score)])
        clusters.append(data_clust)
    pandas.DataFrame(clusters).to_csv(dir_name + csv_file_name + " - " + get_time() + ".csv", index=False,
                                      encoding="ISO-8859-1", sep=';')


def calc_distance(index):
    cluster_index = int(csv_data['cluster_num'][index])
    cluster_centers_offset = reduced_counts.shape[0]
    x_cluster = reduced_counts_plus_centers[cluster_centers_offset + cluster_index][0]
    y_cluster = reduced_counts_plus_centers[cluster_centers_offset + cluster_index][1]
    x_data = reduced_counts_plus_centers[index][0]
    y_data = reduced_counts_plus_centers[index][1]
    return np.sqrt(np.power(abs(x_data - x_cluster), 2) + np.power(abs(y_data - y_cluster), 2))


def print_tsne_plot():
    from matplotlib import pyplot as plt
    x_coords_sample = []
    y_coords_sample = []
    sample_indexes = []
    counts_size = len(reduced_counts)
    if SAMPLE_SIZE >= counts_size:
        x_coords_sample = [count[0] for count in reduced_counts]
        y_coords_sample = [count[1] for count in reduced_counts]
    else:
        for index in range(SAMPLE_SIZE):
            counts_index = random.randint(0, counts_size - 1)
            while sample_indexes.__contains__(counts_index):
                counts_index = random.randint(0, counts_size - 1)
            x_coords_sample.append(reduced_counts[counts_index][0])
            y_coords_sample.append(reduced_counts[counts_index][1])
            sample_indexes.append(counts_index)
    plt.scatter(x_coords_sample, y_coords_sample)
    # plt.xlabel("Number of Clusters")
    # plt.ylabel(eval_score_name)
    plt.title("T-SNE on " + str(number_of_clusters) + " clusters")
    plt.savefig(dir_name + file_name + " - T-SNE on " + str(number_of_clusters) + " clusters - " + get_time() + ".pdf")
    if SHOW:
        plt.show()
    else:
        plt.clf()


# TODO: Adapt this
# Source: http://scikit-learn.org/stable/auto_examples/cluster/plot_kmeans_digits.html
# Used TruncatedSVD instead of PCA to support sparse input
def print_plot():
    global counts
    from matplotlib import pyplot as plt
    counts_len = reduced_counts.shape[0]

    # Step size of the mesh. Decrease to increase the quality of the VQ.
    h = .02  # point in the mesh [x_min, x_max]x[y_min, y_max].
    if vectorization_mode == "glove" or vectorization_mode == "fasttext":
        h = 0.5

    # Plot the decision boundary. For that, we will assign a color to each
    x_min, x_max = min(reduced_counts_plus_centers[:, 0]) - 1, max(reduced_counts_plus_centers[:, 0]) + 1
    y_min, y_max = min(reduced_counts_plus_centers[:, 1]) - 1, max(reduced_counts_plus_centers[:, 1]) + 1
    xx, yy = np.meshgrid(np.arange(x_min, x_max, h), np.arange(y_min, y_max, h))

    distances = []
    x_coords = []
    y_coords = []
    # more_dims = []
    for index in range(counts_len):
        distances.append(calc_distance(index))
        x_coords.append(reduced_counts_plus_centers[index][0])
        y_coords.append(reduced_counts_plus_centers[index][1])
        # local_dims = []
        # for dimension in counts_plus_centers:
        #     local_dims.append(str(dimension) + ", ")
        # more_dims.append(local_dims)
    csv_data['distance'] = distances
    csv_data['x'] = x_coords
    csv_data['y'] = y_coords
    # csv_data['all_dims'] = more_dims

    # Obtain labels for each point in mesh. Use last trained model.
    Z = clustering_alg.predict(np.c_[xx.ravel(), yy.ravel()])

    # Put the result into a color plot
    Z = Z.reshape(xx.shape)
    plt.figure(1)
    plt.clf()
    plt.imshow(Z, interpolation='nearest',
               extent=(xx.min(), xx.max(), yy.min(), yy.max()),
               cmap=plt.cm.Paired,
               aspect='auto', origin='lower')

    plt.plot(reduced_counts_plus_centers[:, 0], reduced_counts_plus_centers[:, 1], 'k.', markersize=2)
    # Plot the centroids as a white X
    centroids = np.array([reduced_counts_plus_centers[i] for i in range(counts_len, counts_len + number_of_clusters)])
    plt.scatter(centroids[:, 0], centroids[:, 1],
                marker='x', s=169, linewidths=3,
                color='w', zorder=10)
    plt.title('K-means clustering (' + str(optimal_number_of_clusters) + ' clusters) on the Ubuntu corpus\n'
              'Centroids are marked with white cross')
    plt.xlim(x_min, x_max)
    plt.ylim(y_min, y_max)
    plt.xticks(())
    plt.yticks(())
    plt.savefig(dir_name + file_name + " - clustering with " + str(number_of_clusters) + " clusters - "
                + get_time() + ".pdf")
    if SHOW:
        plt.show()
    else:
        plt.clf()


def print_alternative_plot():
    from matplotlib import pyplot as plt
    from itertools import cycle

    plt.figure(1)
    plt.clf()

    colors = cycle('bgrcmykbgrcmykbgrcmykbgrcmyk')
    for k, col in zip(range(number_of_clusters), colors):
        my_members = csv_data['cluster_num'] == k
        if clustering_alg_name == "gaussian":
            cluster_center = clustering_alg.means_[k]
        else:
            cluster_center = clustering_alg.cluster_centers_[k]
        plt.plot(reduced_counts[my_members, 0], reduced_counts[my_members, 1], col + '.')
        plt.plot(cluster_center[0], cluster_center[1], 'o', markerfacecolor=col,
                 markeredgecolor='k', markersize=14)
    plt.title('Estimated number of clusters: %d' % number_of_clusters)
    plt.savefig(dir_name + file_name + " - alternative plot - " + str(number_of_clusters) + " clusters - "
                + get_time() + ".pdf")
    if SHOW:
        plt.show()
    else:
        plt.clf()


def print_density_plot():
    import matplotlib.pyplot as plt
    plt.spy(reduced_counts, aspect='auto')
    plt.title("Sparse Matrix")
    plt.savefig(dir_name + file_name + " - density plot - " + str(number_of_clusters) + " clusters - "
                + get_time() + ".pdf")
    if SHOW:
        plt.show()
    else:
        plt.clf()


def fill_scores(eval_score_name):
    scores = []
    global best_number_of_clusters
    global best_cluster_score
    if eval_score_name == "Silhouette":
        scores = silhouette_scores
        best_number_of_clusters = max(scores).__getitem__(1)
        best_cluster_score = max(scores).__getitem__(0)
    elif eval_score_name == "Calinski-Harabaz":
        scores = calinski_harabaz_scores
        best_number_of_clusters = max(scores).__getitem__(1)
        best_cluster_score = max(scores).__getitem__(0)
    elif eval_score_name == "Mean Squared Error":
        scores = mean_squared_errors
        best_number_of_clusters = min(scores).__getitem__(1)
        best_cluster_score = min(scores).__getitem__(0)
    elif eval_score_name == "Intra Cluster Distance":
        scores = intra_cluster_dists
        best_number_of_clusters = min(scores).__getitem__(1)
        best_cluster_score = min(scores).__getitem__(0)
    elif eval_score_name == "Inter Cluster Distance":
        scores = inter_cluster_dists
        best_number_of_clusters = max(scores).__getitem__(1)
        best_cluster_score = max(scores).__getitem__(0)
    elif eval_score_name == "Accuracy":
        scores = accuracy_scores
        best_number_of_clusters = max(scores).__getitem__(1)
        best_cluster_score = max(scores).__getitem__(0)
    return scores


def plot_score_graph(eval_score_name):
    from matplotlib import pyplot as plt
    if eval_score_name != "Intra and Inter Cluster Distance":
        scores = fill_scores(eval_score_name)
        plt.plot([score[1] for score in scores], [score[0] for score in scores])
    else:
        intra_scores = fill_scores("Intra Cluster Distance")
        inter_scores = fill_scores("Inter Cluster Distance")
        intra_label, = plt.plot([score[1] for score in intra_scores], [score[0] for score in intra_scores],
                                label="Intra Cluster Distance")
        inter_label, = plt.plot([score[1] for score in inter_scores], [score[0] for score in inter_scores],
                                label="Inter Cluster Distance")
        plt.legend(handles=[intra_label, inter_label])
    plt.xlabel("Number of Clusters")
    plt.ylabel(eval_score_name)
    plt.title(eval_score_name)
    plt.savefig(dir_name + file_name + " - " + eval_score_name + " scores - " + str(min_number_of_clusters)
                + " to " + str(max_number_of_clusters) + " clusters - " + get_time() + ".pdf")
    if SHOW:
        plt.show()
    else:
        plt.clf()


def get_silhouette():
    return metrics.silhouette_score(reduced_counts, csv_data['cluster_num'], metric='euclidean', sample_size=5000)


def get_calinski_harabaz_score():
    return metrics.calinski_harabaz_score(reduced_counts, csv_data['cluster_num'])


def get_mean_squared_error():
    if clustering_alg_name == "gaussian":
        return 0
    counts_len = reduced_counts.shape[0]
    y_pred = [reduced_counts_plus_centers[counts_len + label] for label in csv_data['cluster_num']]
    return metrics.mean_squared_error(reduced_counts, y_pred)


def get_inter_cluster_distance():
    return src.cluster_similarity.get_mean_inter_clusts(reduced_counts, csv_data['cluster_num'],
                                                    metric='euclidean', sample_size=5000)


def get_intra_cluster_distance():
    return src.cluster_similarity.get_mean_intra_clusts(reduced_counts, csv_data['cluster_num'],
                                                    metric='euclidean', sample_size=5000)


def get_accuracy_score():
    return metrics.accuracy_score(y_pred=csv_data['cluster_num'], y_true=true_labels)


def print_best_cluster_num(eval_score_name):
    fill_scores(eval_score_name)
    print("The best number of clusters is " + str(best_number_of_clusters) + " based on " + eval_score_name + ". "
          + "With a score of " + str(best_cluster_score))
    return best_number_of_clusters


# Source: http://scikit-learn.org/stable/auto_examples/cluster/plot_kmeans_silhouette_analysis.html
def plot_silhouette_scores():
    from matplotlib import pyplot as plt
    samples = random.sample(range(csv_data['cluster_num'].count()), min(csv_data['cluster_num'].count(), 1000))
    data_sample = []
    csv_sample = []
    for index in samples:
        data_sample.append(reduced_counts[index])
        csv_sample.append(csv_data['cluster_num'][index])
    csv_sample = pandas.Series(csv_sample)
    silhouette_scores_local = metrics.silhouette_samples(data_sample, csv_sample)
    y_min = 10
    fig, (ax) = plt.subplots(1, 1)
    from itertools import cycle
    colors = cycle('bgrcmykbgrcmykbgrcmykbgrcmyk')
    for i, col in zip(range(number_of_clusters), colors):
        # Aggregate the silhouette scores for samples belonging to
        # cluster i, and sort them
        scores_cluster_i = silhouette_scores_local[csv_sample == i]
        scores_cluster_i.sort()
        size_cluster_i = scores_cluster_i.shape[0]
        ax.set_xlim([-1, 1])
        y_max = y_min + size_cluster_i
        ax.fill_betweenx(np.arange(y_min, y_max),
                         0, scores_cluster_i,
                         facecolor=col, edgecolor=col, alpha=0.7)

        # Label the silhouette plots with their cluster numbers at the middle
        ax.text(-0.05, y_min + 0.5 * size_cluster_i, str(i))

        # Compute the new y_lower for next plot
        y_min = y_max + 10  # 10 for the 0 samples
    # The vertical line for average silhouette score of all the values
    ax.axvline(x=silhouette_scores[len(silhouette_scores) - 1][0], color="red", linestyle="--")
    ax.set_yticks([])  # Clear the yaxis labels / ticks
    ax.set_xticks([-1.0, -0.8, -0.6, -0.4, -0.2, 0, 0.2, 0.4, 0.6, 0.8, 1])
    plt.suptitle(("Silhouette analysis for KMeans clustering "
                  "with n_clusters = %d" % number_of_clusters),
                 fontsize=12, fontweight='bold')
    plt.savefig(dir_name + file_name + " - " + "Silhouette" + " scores for each context - "
                + str(number_of_clusters) + " clusters - " + get_time() + ".pdf")
    if SHOW:
        plt.show()
    else:
        plt.clf()


def save_vectorizer():
    with open(dir_name + 'vectorizer.pk', 'wb') as fin:
        pickle.dump(vectorizer, fin)


def load_vectorizer(local_dir_name, results_dir="results"):
    with open("../../" + results_dir + "/" + local_dir_name + "vectorizer.pk", 'rb') as pickle_file:
        local_loaded_vectorizer = pickle.load(pickle_file)
    return local_loaded_vectorizer


def save_pca():
    with open(dir_name + 'pca.pk', 'wb') as fin:
        pickle.dump(pca, fin)


def load_pca(local_dir_name, results_dir="results"):
    with open("../../" + results_dir + "/" + local_dir_name + "pca.pk", 'rb') as pickle_file:
        local_loaded_pca = pickle.load(pickle_file)
    return local_loaded_pca


def set_true_labels(labels):
    global true_labels
    true_labels = labels


def get_true_labels():
    return true_labels


def set_reduced_counts(red_counts):
    global reduced_counts
    reduced_counts = red_counts


def read_clustered_file(local_file_name):
    global csv_data
    local_nums = csv_data['cluster_num']
    csv_data = pandas.read_csv(local_file_name, encoding="ISO-8859-1", sep=';')
    for i, num in enumerate(local_nums):
        csv_data.set_value(i, 'cluster_num', num)


def fix_task_indexes(cluster_centers=None, local_file_name=""):
    global true_labels
    global reduced_counts_plus_centers
    global vectorization_mode
    global correct_ubuntu_index
    global correct_weather_index
    global correct_schedule_index
    global correct_navigate_index
    correct_ubuntu_index = ubuntu_index
    correct_weather_index = weather_index
    correct_schedule_index = schedule_index
    correct_navigate_index = navigate_index
    if cluster_centers is not None:
        enumerater = enumerate(cluster_centers)
        read_clustered_file(local_file_name=local_file_name)
        reduced_counts_plus_centers = np.append(reduced_counts, cluster_centers, axis=0)
        vectorization_mode = "tf-idf"
    elif clustering_alg_name == "gaussian":
        enumerater = enumerate(clustering_alg.means_)
    else:
        enumerater = enumerate(clustering_alg.cluster_centers_)

    for index, center in enumerater:
        index_of_nearest_dialog = get_nearest_dialog(index)
        if len(nearest_dialogs) == 0:
            if vectorization_mode == "tf-idf":
                data_cluster = pandas.concat([pandas.Series(
                    vectorizer.steps[0][1].transformer_list[0][1].get_feature_names()), pandas.DataFrame(
                        counts[index_of_nearest_dialog].toarray().transpose())], axis=1)
            elif vectorization_mode == "count":
                data_cluster = pandas.concat([pandas.Series(vectorizer.get_feature_names()), pandas.DataFrame(
                        counts[index_of_nearest_dialog].toarray().transpose())], axis=1)
            data_cluster.columns = ['keywords', 'weights']
            data_cluster = data_cluster.sort_values(by=['weights'], ascending=False)
            data_clust = data_cluster.head(n=10)['keywords'].tolist()
            data_clust_keywords = ' '.join(data_clust)
            if data_clust_keywords.find("store") != -1 or data_clust_keywords.find("university") != -1\
                    or data_clust_keywords.find("navigate") != -1 or data_clust_keywords.find("station") != -1\
                    or data_clust_keywords.find("gas") != -1 or data_clust_keywords.find("closest") != -1\
                    or data_clust_keywords.find("park") != -1 or data_clust_keywords.find("parking") != -1\
                    or data_clust_keywords.find("traffic") != -1 or data_clust_keywords.find("fastest road") != -1\
                    or data_clust_keywords.find("nearest is") != -1 or data_clust_keywords.find("directions") != -1\
                    or data_clust_keywords.find("shortest route") != -1 or data_clust_keywords.find("oval parking") != -1\
                    or data_clust_keywords.find("dish parking") != -1 or data_clust_keywords.find("navigating") != -1:
                correct_navigate_index = index
                print("NAVIGATE: " + '; '.join([word for word in data_clust]))
            elif data_clust_keywords.find("weather") != -1 or data_clust_keywords.find("temperature") != -1\
                    or data_clust_keywords.find("rain") != -1 or data_clust_keywords.find("raining") != -1\
                    or data_clust_keywords.find("weath") != -1 or data_clust_keywords.find("cloud") != -1\
                    or data_clust_keywords.find("cloudy") != -1 or data_clust_keywords.find("low") != -1\
                    or data_clust_keywords.find("high") != -1 or data_clust_keywords.find("humid wednesday") != -1\
                    or data_clust_keywords.find("be cloudy") != -1 or data_clust_keywords.find("mountain view") != -1\
                    or data_clust_keywords.find("the temperature") != -1 or data_clust_keywords.find("cold") != -1\
                    or data_clust_keywords.find("high temperature") != -1 or data_clust_keywords.find("forecast") != -1:
                correct_weather_index = index
                print("WEATHER: " + '; '.join([word for word in data_clust]))
            elif data_clust_keywords.find("schedule") != -1 or data_clust_keywords.find("appointment") != -1\
                    or data_clust_keywords.find("appointments") != -1 or data_clust_keywords.find("date") != -1\
                    or data_clust_keywords.find("dates") != -1 or data_clust_keywords.find("activity") != -1\
                    or data_clust_keywords.find("pm") != -1 or data_clust_keywords.find("set reminder") != -1\
                    or data_clust_keywords.find("been scheduled") != -1 or data_clust_keywords.find("schedule football") != -1\
                    or data_clust_keywords.find("swimming activities") != -1 or data_clust_keywords.find("for conference") != -1\
                    or data_clust_keywords.find("remind") != -1 or data_clust_keywords.find("time") != -1\
                    or data_clust_keywords.find("distance") != -1:
                correct_schedule_index = index
                print("SCHEDULE: " + '; '.join([word for word in data_clust]))
            else:
                correct_ubuntu_index = index
                print("UBUNTU: " + '; '.join([word for word in data_clust]))
    for index, label in enumerate(true_labels):
        if label == ubuntu_index:
            try:
                true_labels.set_value(index, correct_ubuntu_index)
            except AttributeError:
                true_labels[index] = correct_ubuntu_index
        elif label == weather_index:
            try:
                true_labels.set_value(index, correct_weather_index)
            except AttributeError:
                true_labels[index] = correct_weather_index
        elif label == schedule_index:
            try:
                true_labels.set_value(index, correct_schedule_index)
            except AttributeError:
                true_labels[index] = correct_schedule_index
        elif label == navigate_index:
            try:
                true_labels.set_value(index, correct_navigate_index)
            except AttributeError:
                true_labels[index] = correct_navigate_index


def do_clustering(no_clusters, to_return=False):
    global silhouette_score
    global calinski_harabaz_score
    global mean_squared_error
    global intra_cluster_dist
    global inter_cluster_dist
    global accuracy_score
    global silhouette_scores
    global calinski_harabaz_scores
    global mean_squared_errors
    global intra_cluster_dists
    global inter_cluster_dists
    global accuracy_scores
    global nearest_dialogs_cluster_nums
    print("starting with " + str(no_clusters) + " clusters...")
    if clustering_alg_name == "meanshift" or clustering_alg_name == "kmeans":
        clustering_alg.n_clusters = no_clusters
    elif clustering_alg_name == "gaussian":
        clustering_alg.n_components = no_clusters
    # clustering_alg_not_reduced.n_clusters = no_clusters
    clustering_alg.fit(reduced_counts)
    predicted_nums = clustering_alg.predict(reduced_counts)
    if to_return:
        # csv_data['cluster_num'] = predicted_nums  # [4:len(predicted_nums)]
        nearest_dialogs_cluster_nums = [predicted_nums[index] for index in nearest_dialogs]  # predicted_nums[0:4]
        return predicted_nums
    csv_data['cluster_num'] = predicted_nums
    covariances = clustering_alg.covariances_[0]
    # clustering_alg_not_reduced.fit_predict(counts)
    reduce_counts_plus_centers()
    # TODO: Do the same for different modes without vectorizer!
    if vectorization_mode == "tf-idf" or vectorization_mode == "count":
        fix_task_indexes()
    if to_evaluate:
        print("computing evaluation scores for %d clusters..." % no_clusters)
        silhouette_score = get_silhouette()
        silhouette_scores.append((silhouette_score, number_of_clusters))
        calinski_harabaz_score = get_calinski_harabaz_score()
        calinski_harabaz_scores.append((calinski_harabaz_score, number_of_clusters))
        mean_squared_error = get_mean_squared_error()
        mean_squared_errors.append((mean_squared_error, number_of_clusters))
        intra_cluster_dist = get_intra_cluster_distance()
        intra_cluster_dists.append((intra_cluster_dist, number_of_clusters))
        inter_cluster_dist = get_inter_cluster_distance()
        inter_cluster_dists.append((get_inter_cluster_distance(), number_of_clusters))
        accuracy_score = get_accuracy_score()
        accuracy_scores.append((accuracy_score, number_of_clusters))
        plot_silhouette_scores()


def get_user_input():
    global SCRIPTED
    global min_number_of_clusters
    global max_number_of_clusters
    global step_size
    global vectorization_mode
    global LOG
    global file_name
    global clustering_alg_name
    log_choice = input("Do you want to activate logging? (y/n)\n")
    if log_choice == "input.txt":
        SCRIPTED = True
        input_file = open(log_choice, 'r')
        log_choice = input_file.readline().rstrip('\n')
        file_choice = int(input_file.readline().rstrip('\n'))
        clustering_alg_name = input_file.readline().rstrip('\n')
        if clustering_alg_name == "k" or clustering_alg_name == "kmeans":
            clustering_alg_name = "kmeans"
            min_number_of_clusters = int(input_file.readline().rstrip('\n'))
            max_number_of_clusters = int(input_file.readline().rstrip('\n'))
            step_size = int(input_file.readline().rstrip('\n'))
        elif clustering_alg_name == "m":
            clustering_alg_name = "meanshift"
        elif clustering_alg_name == "g":
            clustering_alg_name = "gaussian"
            min_number_of_clusters = int(input_file.readline().rstrip('\n'))
            max_number_of_clusters = int(input_file.readline().rstrip('\n'))
        vectorization_mode = input_file.readline().rstrip('\n').lower()
    else:
        file_choice = int(input("What data? 0 = train-new, 1 = test-new, 2 = valid-new, 3 = train, "
                                "4 = test-1-distractor, 5 = test-1-distractor-first-100, 6 = kvret_test_public, "
                                "7 = kvret_train_public, 8 = kvret_dev_public. Other input = exit\n"))
    possible_file_choices = ["train-new", "test-new", "valid-new", "train", "test-1-distractor",
                             "test-1-distractor-first-100", "kvret_test_public", "kvret_train_public",
                             "kvret_dev_public"]
    if log_choice == "y":
        LOG = True
    if len(possible_file_choices) > file_choice >= 0:
        file_name = possible_file_choices[file_choice]  # "-converted"
    else:
        exit()
    print(file_name)
    if not SCRIPTED:
        clustering_alg_name = input("Which clustering algorithm do you want to use? (K)means, (M)eanshift or (G)aussian "
                                    "Mixture?\n") \
            .lower()
        if clustering_alg_name == "k":
            clustering_alg_name = "kmeans"
        elif clustering_alg_name == "m":
            clustering_alg_name = "meanshift"
        elif clustering_alg_name == "g":
            clustering_alg_name = "gaussian"
        print(clustering_alg_name)
        if clustering_alg_name == "kmeans" or clustering_alg_name == "gaussian":
            min_number_of_clusters = int(input("How many clusters do you want to generate at minimum?\n"))
            print(min_number_of_clusters)
            max_number_of_clusters = int(input("How many clusters do you want to generate at maximum?\n"))
            print(max_number_of_clusters)
            step_size = int(input("What should the step size be?\n"))
            print(step_size)
        vectorization_mode = input("Which vectorization mode should be used? (T)F-IDF, (C)ount, (D)oc2Vec, "
                                   "(G)loVe, (F)astText or (W)ikipedia2Vec?\n").lower()
    possible_vectoritaion_choices = ["tf-idf", "count", "doc2vec", "glove", "fasttext", "wikipedia2vec"]
    for vectorization_name in possible_vectoritaion_choices:
        if vectorization_mode == vectorization_name[0]:
            vectorization_mode = vectorization_name
    print(vectorization_mode)


def do_with_time_output(f, *args):
    start_time = datetime.datetime.now()
    result = []
    if len(args) == 1:
        result = f(args[0])
    elif len(args) == 2:
        result = f(args[0], args[1])
    elif len(args) == 3:
        result = f(args[0], args[1], args[2])
    elif len(args) == 4:
        result = f(args[0], args[1], args[2], args[3])
    time_elapsed = datetime.datetime.now() - start_time
    print('Time elapsed (hh:mm:ss.ms) {}'.format(time_elapsed))
    return result


if __name__ == '__main__':
    get_user_input()
    dir_name = file_name + " with " + vectorization_mode + " - " + get_time()
    dir_name = "../../results/" + dir_name + "/"
    if not os.path.exists(dir_name):
        os.makedirs(dir_name)
    ubuntu_data = pandas.DataFrame(clean_words((read_from_csv(file_name, only_first_utterance=False))), columns=["clean_sum", "sum"])
    json_file = open("../../data/" + file_name + '.json')
    json_data = json.load(json_file)
    print("START TO CLEAN WORDS")
    csv_data = do_with_time_output(clean_words_json, json_data, False)
    csv_data = pandas.concat([ubuntu_data, csv_data], axis=0, ignore_index=True, sort=False)
    print("FINISHED CLEANING WORDS AND STARTING TO FILL TEXTCOLLECTION")
    if to_fill_textcollection:
        my_dialogs = TextCollection([dialog for dialog in csv_data['clean_sum']])
    print("FINISHED FILLING TEXTCOLLECTION AND STARTING VECTORIZING")
    do_with_time_output(vectorize_words, vectorization_mode)
    print("FINISHED VECTORIZING")
    do_with_time_output(reduce_counts())
    silhouette_scores = []
    calinski_harabaz_scores = []
    mean_squared_errors = []
    intra_cluster_dists = []
    inter_cluster_dists = []
    accuracy_scores = []
    optimal_number_of_clusters = 0
    if clustering_alg_name == "kmeans" or clustering_alg_name == "gaussian":
        if clustering_alg_name == "kmeans":
            clustering_alg = KMeans(init='k-means++', n_clusters=30, n_init=10, n_jobs=-1)
        elif clustering_alg_name == "gaussian":
            clustering_alg = GaussianMixture(n_components=30, n_init=10)
        # clustering_alg_not_reduced = KMeans(init='k-means++', n_clusters=30, n_init=10, n_jobs=-1)
        print("CREATED KMEANS")
        for number_of_clusters in range(min_number_of_clusters, max_number_of_clusters + 1, step_size):
            do_with_time_output(do_clustering, number_of_clusters)
        if to_evaluate:
            optimal_number_of_clusters = print_best_cluster_num("Silhouette")
        else:
            optimal_number_of_clusters = max_number_of_clusters
            print("Because no evaluation was done, the maximum number of clusters will be saved")
        clustering_alg.n_clusters = optimal_number_of_clusters
        # clustering_alg_not_reduced.n_clusters = optimal_number_of_clusters
    elif clustering_alg_name == "meanshift":
        clustering_alg = MeanShift(n_jobs=-1)
        print("CREATED MEANSHIFT")
    print("starting with " + str(optimal_number_of_clusters) + " clusters...")
    if clustering_alg_name == "meanshift" or max_number_of_clusters != min_number_of_clusters:
        csv_data['cluster_num'] = do_with_time_output(clustering_alg.fit_predict, reduced_counts)
    if clustering_alg_name == "gaussian":
        number_of_clusters = len(clustering_alg.means_)
    else:
        number_of_clusters = len(clustering_alg.cluster_centers_)
    if number_of_clusters < 2:
        print("Only 1 cluster could be found. Stopping calculations.")
        exit(1)
    do_with_time_output(reduce_counts_plus_centers())
    if to_evaluate and clustering_alg_name == "meanshift":
        print("computing evaluation scores for %d clusters..." % number_of_clusters)
        silhouette_score = get_silhouette()
        silhouette_scores.append((silhouette_score, number_of_clusters))
        calinski_harabaz_score = get_calinski_harabaz_score()
        calinski_harabaz_scores.append((calinski_harabaz_score, number_of_clusters))
        mean_squared_error = get_mean_squared_error()
        mean_squared_errors.append((mean_squared_error, number_of_clusters))
        intra_cluster_dist = get_intra_cluster_distance()
        intra_cluster_dists.append((intra_cluster_dist, number_of_clusters))
        inter_cluster_dist = get_inter_cluster_distance()
        inter_cluster_dists.append((inter_cluster_dist, number_of_clusters))
        accuracy_score = get_accuracy_score()
        accuracy_scores.append((accuracy_score, number_of_clusters))
        plot_silhouette_scores()
    if to_evaluate:
        print_best_cluster_num("Silhouette")
        print_best_cluster_num("Calinski-Harabaz")
        print_best_cluster_num("Mean Squared Error")
        print_best_cluster_num("Intra Cluster Distance")
        print_best_cluster_num("Inter Cluster Distance")
        print_best_cluster_num("Accuracy")
        plot_score_graph("Silhouette")
        plot_score_graph("Calinski-Harabaz")
        plot_score_graph("Mean Squared Error")
        plot_score_graph("Intra Cluster Distance")
        plot_score_graph("Inter Cluster Distance")
        plot_score_graph("Intra and Inter Cluster Distance")
        plot_score_graph("Accuracy")
    print_plot()
    print("writing " + str(optimal_number_of_clusters) + " clusters to disk...")
    do_with_time_output(write_clusters_to_csv, file_name + " - Attributes of " + str(number_of_clusters)
                        + " Clusters with " + vectorization_mode)
    csv_data['actual_cluster'] = true_labels
    do_with_time_output(write_source_to_csv, csv_data, file_name + " - " + str(number_of_clusters) + " Clusters with "
                        + vectorization_mode, dir_name)
    if vectorization_mode == "tf-idf" or vectorization_mode == "count":
        save_vectorizer()
    save_pca()
    print_alternative_plot()
    print_density_plot()
    if CREATE_TSNE:
        print("Plotting T-SNE...")
        print_tsne_plot()
    print("done")
