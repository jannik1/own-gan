import argparse
import csv
import io
import json

import pandas

import spacy

import numpy as np
import tensorflow as tf
from tensorflow.contrib import rnn

from src import my_clustering, data_statistics
from src.my_clustering import ubuntu_index
from src.reader import Vocabulary

mode = "weather"
used_indexes = [1]
kb = ""  # " - kb"
inputs_int = []
batch_size = 256
nd_steps = 10
ng_steps = 10
spacy_nlp = spacy.load('en')
to_plot = False
word_based = True  # generate on word basis or on sentence basis?
if word_based:
    tensor_size = 386
else:
    tensor_size = 7701
tensor_size = 20
num_input = 20  # MNIST data input (img shape: 28*28)
num_hidden = 128  # hidden layer num of features
timesteps = 20  # timesteps
num_classes = 10  # MNIST total classes (0-9 digits)


def BiRNNGenerator(x, weights, biases, reuse=False):
    with tf.variable_scope("GAN/Generator", reuse=reuse):
        # Prepare data shape to match `rnn` function requirements
        # Current data input shape: (batch_size, timesteps, n_input)
        # Required shape: 'timesteps' tensors list of shape (batch_size, num_input)

        # Unstack to get a list of 'timesteps' tensors of shape (batch_size, num_input)
        x = tf.unstack(x, timesteps, 1)

        # Define lstm cells with tensorflow
        # Forward direction cell
        lstm_fw_cell = rnn.BasicLSTMCell(num_hidden, forget_bias=1.0)
        # Backward direction cell
        lstm_bw_cell = rnn.BasicLSTMCell(num_hidden, forget_bias=1.0)

        # Get lstm cell output
        try:
            outputs, _, _ = rnn.static_bidirectional_rnn(lstm_fw_cell, lstm_bw_cell, x,
                                                         dtype=tf.float32)
        except Exception:  # Old TensorFlow version only returns outputs not states
            outputs = rnn.static_bidirectional_rnn(lstm_fw_cell, lstm_bw_cell, x,
                                                   dtype=tf.float32)

        # Linear activation, using rnn inner loop last output
        return tf.matmul(outputs[-1], weights['out']) + biases['out']


def BiRNNDiscriminator(x, weights, biases, reuse=False):
    with tf.variable_scope("GAN/Discriminator", reuse=reuse):
        # Prepare data shape to match `rnn` function requirements
        # Current data input shape: (batch_size, timesteps, n_input)
        # Required shape: 'timesteps' tensors list of shape (batch_size, num_input)

        # Unstack to get a list of 'timesteps' tensors of shape (batch_size, num_input)
        x = tf.unstack(x, timesteps, 1)

        # Define lstm cells with tensorflow
        # Forward direction cell
        lstm_fw_cell = rnn.BasicLSTMCell(num_hidden, forget_bias=1.0)
        # Backward direction cell
        lstm_bw_cell = rnn.BasicLSTMCell(num_hidden, forget_bias=1.0)

        # Get lstm cell output
        try:
            outputs, _, _ = rnn.static_bidirectional_rnn(lstm_fw_cell, lstm_bw_cell, x,
                                                         dtype=tf.float32)
        except Exception:  # Old TensorFlow version only returns outputs not states
            outputs = rnn.static_bidirectional_rnn(lstm_fw_cell, lstm_bw_cell, x,
                                                   dtype=tf.float32)

        # Linear activation, using rnn inner loop last output
        return tf.matmul(outputs[-1], weights['out']) + biases['out']


def get_ng_steps():
    return ng_steps


def get_nd_steps():
    return nd_steps


def get_batch_size():
    return batch_size


def get_inputs_int():
    return inputs_int


def convert_dialogue_to_vecs(dialogue):
    dialogue = dialogue.split(' ')
    for word_index, word in enumerate(dialogue):
        dialogue[word_index] = word.strip('?').strip('!').strip('.').strip(',').strip(';').strip(':').strip(' ')
    dialogue = list(filter(lambda x: x != ' ', dialogue))
    dialogue = list(filter(lambda x: x != '', dialogue))
    unk_vec = spacy_nlp("<unk>").vector
    unk_vec_norm = spacy_nlp("<unk>").vector_norm
    tab_vec = spacy_nlp("<tab>").vector
    tab_vec_norm = spacy_nlp("<tab>").vector_norm
    vecs = []
    for i in range(20):
        vecs.extend(tab_vec)
        vecs.append(tab_vec_norm)
    eos_vector = spacy_nlp("<eos>").vector
    for i in range(384):
        vecs[385 * 19 + i] = eos_vector[i]
    vecs[385 * 19 + 384] = spacy_nlp("<eos>").vector_norm
    unks = 0
    for i in range(len(dialogue)):
        if i < 19:
            try:
                word_vec_list = word_to_vec[
                    dialogue[len(dialogue) - i - 1].strip('?').strip('!').strip('.').strip(',').strip(';').strip(
                        ':').lower()]
                for vec_i in range(384):
                    vecs[385 * (18 - i) + vec_i] = word_vec_list[0][vec_i]
                vecs[385 * (18 - i) + 384] = word_vec_list[1]
            except KeyError:
                unks += 1
                print("UNK")
                print(dialogue[len(dialogue) - i - 1])
                for vec_i in range(384):
                    vecs[385 * (18 - i) + vec_i] = unk_vec[vec_i]
                vecs[385 * (18 - i) + 384] = unk_vec_norm
    if unks > 0:
        print("Number of unks: " + str(unks))
    return vecs


def load_vocab_inputs():
    global inputs_int, batch_size, nd_steps, ng_steps, vocab, word_to_vec
    parser = argparse.ArgumentParser()
    named_args = parser.add_argument_group('named arguments')

    named_args.add_argument('-e', '--epochs', metavar='|',
                            help="""Number of Epochs to Run""",
                            required=False, default=130, type=int)
    named_args.add_argument('-es', '--embedding', metavar='|',
                            help="""Size of the embedding""",
                            required=False, default=200, type=int)

    named_args.add_argument('-g', '--gpu', metavar='|',
                            help="""GPU to use""",
                            required=False, default='1', type=str)

    named_args.add_argument('-p', '--padding', metavar='|',
                            help="""Amount of padding to use""",
                            required=False, default=20, type=int)

    named_args.add_argument('-t', '--training-data', metavar='|',
                            help="""Location of training data""",
                            required=False, default='../../../KVMemnn-jannik/data/train_data_ - full - kb.csv')

    named_args.add_argument('-v', '--validation-data', metavar='|',
                            help="""Location of validation data""",
                            required=False, default='./data/val_data.csv')

    named_args.add_argument('-b', '--batch-size', metavar='|',
                            help="""Location of validation data""",
                            required=False, default=256, type=int)

    named_args.add_argument('-voc', '--vocabulary-data', metavar='|',
                            help="""Location of vocabulary file""",
                            required=False, default='../../../KVMemnn-jannik/data/vocabulary.json')
    args = parser.parse_args()
    print(args)

    batch_size = args.batch_size
    # training_data = args.training_data
    vocabulary_data = args.vocabulary_data
    if mode.strip(" ") != "":
        # if kb == "":
        #     training_data = "../../../KVMemnn-jannik/data/train_data - " + mode + kb + ".csv"
        # else:
        #     training_data = "../../../KVMemnn-jannik/data/train_data_ - " + mode + kb + ".csv"
        vocabulary_data = "../../../KVMemnn-jannik/data/vocabulary - " + mode + ".json"
    ubuntu_data = pandas.DataFrame(
        my_clustering.clean_words((my_clustering.read_from_csv("train-new-converted-min", True))),
        columns=["clean_sum", "sum"])
    file_name = "kvret_train_public"
    try:
        with open("../../../data/" + file_name + '.json') as json_file:
            json_data = json.load(json_file)
    except FileNotFoundError:
        with open("../../data/" + file_name + '.json') as json_file:
            json_data = json.load(json_file)
    data_statistics.set_true_labels([ubuntu_index] * 800)
    train_data = data_statistics.clean_words_json(json_data, file_name, True)
    train_data = pandas.concat([ubuntu_data, train_data], axis=0, ignore_index=True, sort=False)
    true_labels = data_statistics.get_true_labels()
    inputs = [dialogue for index, dialogue in enumerate(train_data['sum'])
              if used_indexes.__contains__(true_labels[index])]

    # loading vocab and dataset
    try:
        vocab = Vocabulary(vocabulary_data,
                           padding=args.padding)
    except FileNotFoundError:
        vocabulary_data = "../../KVMemnn-jannik/data/vocabulary - " + mode + ".json"
        vocab = Vocabulary(vocabulary_data,
                           padding=args.padding)
    word_to_vec = {}
    for i in range(vocab.size()):
        word = vocab.int_to_string([i])
        word = word[len(word) - 2]
        doc = spacy_nlp(word)
        vector = (doc.vector, doc.vector_norm)
        word_to_vec[word] = vector
    # inputs = []
    # targets = []
    # indexes_in_dialogs = []
    # if kb == "":
    #     row_indexes = [1, 2, 5]
    # else:
    #     row_indexes = [0, 1]
    # with io.open(training_data, 'r', encoding="ISO-8859-1") as f:
    #     reader = csv.reader(f, delimiter=';')
    #     for index, row in enumerate(reader):
    #         if not (index == 0 and (row[row_indexes[0]] == "inputs" or row[row_indexes[0]] == "input")) \
    #                 and (len(row_indexes) < 3 or not indexes_in_dialogs.__contains__(int(row[row_indexes[2]]))):
    #             inputs.append(row[row_indexes[0]])
    #             targets.append(row[row_indexes[1]])
    #             indexes_in_dialogs.append(int(row[row_indexes[2]]))
    inputs_int = [convert_dialogue_to_vecs(dialogue) for dialogue in inputs]
    batch_size = int(len(inputs_int) * 0.1)


def get_word_to_vec():
    return word_to_vec


def get_vocab():
    return vocab


def get_vocab_len():
    return vocab.size()


def get_word_y(x):
    dialog = np.random.randint(len(inputs_int))
    return inputs_int[dialog][x * 385: (x + 1) * 385]


def get_y(x):
    # dialog = np.random.randint(len(inputs_int))
    return inputs_int[x]


def sample_data(n=10000, scale=20):
    data = []

    if word_based:
        scale = 20

    x = list(map(int, (list(map(round, (scale * (np.random.random_sample((n,))) - 0.5))))))

    for i in range(n):
        if word_based:
            yi = get_word_y(x[i])
        else:
            yi = get_y(x[i])
        dialogue = [x[i]]
        reverse_dialogue = []
        for y in yi:
            dialogue.append(y)
        for y in reversed(yi):
            reverse_dialogue.append(y)
        reverse_dialogue.append(x[i])
        data.append([dialogue, reverse_dialogue])
        # data.append([x[i], yi[0], yi[1], yi[2], yi[3], yi[4], yi[5], yi[6], yi[7], yi[8], yi[9],
        #              yi[10], yi[11], yi[12], yi[13], yi[14], yi[15], yi[16], yi[17], yi[18], yi[19]])

    return np.array(data)
