import json
import numpy
import re
import random

input_lines = []
input_file = "log-1112-real-"
start_temp = 0.0
end_temp = 0.1
to_sub = False

events = []
times = []
dates = []
parties = []
rooms = []
agendas = []
locations = []

weekly_times = []
temperatures = []
weather_attributes = []

traffic_infos = []
poi_addresses =[]
poi_pois = []
poi_types = []
distances = []

dictionary = {"event": events, "time": times, "date": dates, "party": parties, "room": rooms, "agenda": agendas,
              "location": locations, "weekly_time": weekly_times, "weekly-time": weekly_times, "temperature": temperatures,
              "weather_attribute": weather_attributes, "traffic_info": traffic_infos, "poi_address": poi_addresses,
              "poi_poi": poi_pois, "poi_type": poi_types, "distance": distances}


def load_kb():
    global dictionary
    kb_vocab_file_name = "../../data/kvret_entities_cleaning.json"
    try:
        with open(kb_vocab_file_name, 'r', encoding='utf-8') as f:
            loaded_file = json.load(f)
    except FileNotFoundError:
        with open("../" + kb_vocab_file_name, 'r', encoding='utf-8') as f:
            loaded_file = json.load(f)
    for tag in loaded_file:
        print(tag)
        if tag == "poi":
            for poi in loaded_file[tag]:
                dictionary["poi_address"].append(poi["address"])
                dictionary["poi_poi"].append(poi["poi"])
                dictionary["poi_type"].append(poi["type"])
        else:
            for real_tag in loaded_file[tag]:
                dictionary[tag].append(real_tag)


def load_lines(mode):
    with open(input_file + mode + "-dialogues.txt", 'r') as file:
        read_lines = file.readlines()
    return read_lines


def preprocess_lines(lines, start_temp, end_temp, length):
    out_lines = []
    last_index = 0
    # first_index = lines.index("Temperature: " + start_temp)  # '####################\nTemperature: 0.3\n####################'
    # last_index = lines.index("Temperature: " + end_temp)
    # out_lines.append(lines[first_index:last_index])
    for i in range(length):
        lines = lines[last_index + 2:]
        first_index = lines.index("Temperature: " + start_temp + "\n")
        if i == length - 1 and start_temp == "1.0":
            last_index = len(lines)
        else:
            last_index = lines.index("Temperature: " + end_temp + "\n")
        out_lines.append(lines[first_index + 2:last_index - 2])
    return out_lines


def sub_token(token, text, knowledgebase, last_substitutes):
    if token == "weekly_time" and "date" in last_substitutes:
        substitute = last_substitutes["date"]
    elif token == "date" and "weekly_time" in last_substitutes:
        substitute = last_substitutes["weekly_time"]
    elif token in last_substitutes:
        substitute = last_substitutes[token]
    else:
        substitute_list = dictionary[token]
        rnd_number = random.randint(0, len(substitute_list) - 1)
        substitute = substitute_list[rnd_number]
        last_substitutes[token] = substitute
    text = re.sub("<" + token + ">", substitute, text)
    knowledgebase[token] = substitute
    return text, knowledgebase, last_substitutes


def check_dialogue_turns(line):
    starts = [0]
    start_names = ["driver", "car"]
    index = 0
    original_line = line
    turn_no = 0
    while re.match(".*" + start_names[index % 2] + "[0-9]+:.*", line[starts[index]:]):
        if turn_no > 10:
            print("LONG")
        turn_no +=1
        start = -1
        turn_index = 0
        lowest_start = 999999
        found_turn = -1
        while lowest_start == 9999 or turn_index < 15:
            start = line[starts[index]:].find(start_names[index % 2] + str(turn_index))
            if start != -1 and start < lowest_start:
                lowest_start = start
                found_turn = turn_index
            turn_index += 1
        lowest_start += len(start_names[index % 2]) + len(str(found_turn)) + 2  # re.match(".*" + start_names[turn_status] + str(index) + ".*", line[starts[turn_status]:]).group(0)
        # fix numbers
        # if line[starts[index]:].find(start_names[index % 2] + str(index)) == -1:
        #     line = line[:starts[index]] + re.sub(start_names[index % 2] + "[0-9]+:", start_names[index % 2] + str(index) + ":", line[starts[index]:])
        starts.append(lowest_start + starts[index])
        index += 1
    # check if last utterance is car:
    if index % 2 != 0:
        line = ""
    # remove duplicate utterances (e.g. "driver0: hello driver0: hey" -> "driver0: hello")
    for i, start in enumerate(starts):
        if i != 0:
            if i + 1 < len(starts) and line[start:starts[i + 1]].find(start_names[(i+1) % 2] + str(i - 1) + ":") != -1\
              or i + 1 == len(starts) and line[start:].find(start_names[(i+1) % 2] + str(i - 1) + ":") != -1:
                if i + 1 == len(starts):
                    line_snippet = line[start:]
                else:
                    line_snippet = line[start:starts[i + 1]]
                turn_duplicate_index = line_snippet.find(start_names[(i+1) % 2] + str(i - 1) + ":")
                if i + 1 == len(starts):
                    line = line[:turn_duplicate_index + start]
                else:
                    line = line[:turn_duplicate_index + start] + line[starts[i+1]:]
    if re.match(".*(driver|car)[0-9]+:.*", line[starts[-1]:]):
        line = ""
    dialogue = []
    for st_index, st in enumerate(starts):
        if st_index == len(starts) - 1:
            utterance = original_line[st:-1]
            utterance = re.sub("car[0-9]+:.*", "", utterance)
            dialogue.append(utterance)
        elif st_index != 0 and st_index % 2 == 1:
            utterance = original_line[st:starts[st_index + 1] - 7]
            utterance = re.sub("driver[0-9]+:.*", "", utterance)
            dialogue.append(utterance)
        elif st_index != 0 and st_index % 2 == 0:
            utterance = original_line[st:starts[st_index + 1] - 10]
            utterance = re.sub("car[0-9]+:.*", "", utterance)
            dialogue.append(utterance)
    if len(dialogue) > 10:
        print("LONG")
    return dialogue


def add_kb(dialogue, topic, knowledgebase):
    dialogue_json = ""
    # TODO: Dialogue info is from navigation type, could be adapted too, but might not be necessary
    for index, utterance in enumerate(dialogue):
        utterance = re.sub("\n", "", utterance)
        utterance = re.sub("\t", "", utterance)
        utterance = re.sub("\"", "", utterance)
        if index == len(dialogue) - 1:
            end_dialogue = "true"
        else:
            end_dialogue = "false"
        if index == 0:
            dialogue_json += '"dialogue": [\n      {\n        "turn": "driver",\n        "data": {\n          ' \
                            '"end_dialogue": ' + end_dialogue + ',\n          "utterance": "' + utterance + '"\n        }\n      }'
        # car utterance:
        elif index % 2 == 1:
            dialogue_json += ',\n      {\n        "turn": "assistant",\n        "data": {\n          "end_dialogue": ' + end_dialogue + ',\n' \
                            '          "requested": {\n            "distance": false,\n            "traffic_info": false,\n' \
                            '            "poi_type": true,\n            "address": false,\n            "poi": false\n          },\n' \
                            '          "slots": {\n            "poi_type": "gas stations"\n          },\n' \
                            '          "utterance": "' + utterance + '"\n        }\n      }'
        # driver utterance:
        elif index % 2 == 0:
            dialogue_json += ',\n      {\n        "turn": "driver",\n        "data": {\n          "end_dialogue": ' + end_dialogue + ',\n' \
                            '          "utterance": "' + utterance + '"\n        }\n      }'
    if topic == "schedule":
        dialogue_json += '\n    ],\n    "scenario": {\n      "kb": {\n        "items": [\n          ' \
                                        '{\n            "room": "-",\n            "agenda": "-",\n            "time": "1 pm",\n            ' \
                                        '"date": "monday",\n            "party": "-",\n            "event": "medicine"\n          },\n          ' \
                                        '{\n            "room": "-",\n            "agenda": "-",\n            "time": "11 am",\n            ' \
                                        '"date": "friday",\n            "party": "-",\n            "event": "doctor appointment"\n          },\n          ' \
                                        '{\n            "room": "-",\n            "agenda": "-",\n            "time": "5 pm",\n            ' \
                                        '"date": "thursday",\n            "party": "-",\n            "event": "dinner"\n          },\n          ' \
                                        '{\n            "room": "-",\n            "agenda": "-",\n            "time": "11 am",\n            ' \
                                        '"date": "monday",\n            "party": "-",\n            "event": "dentist"\n          },\n          ' \
                                        '{\n            "room": "-",\n            "agenda": "-",\n            "time": "7 pm",\n            ' \
                                        '"date": "tuesday",\n            "party": "-",\n            "event": "yoga activity"\n          },\n          ' \
                                        '{\n            "room": "' + knowledgebase["room"] +\
                                        '",\n            "agenda": "' + knowledgebase["agenda"] +\
                                        '",\n            "time": "' + knowledgebase["time"] + '",\n            ' \
                                        '"date": "' + knowledgebase["date"] + '",\n            ' \
                                        '"party": "' + knowledgebase["party"] + '",\n            ' \
                                        '"event": "' + knowledgebase["event"] + '"\n          },\n          ' \
                                        '{\n            "room": "-",\n            "agenda": "-",\n            "time": "2 pm",\n            ' \
                                        '"date": "tuesday",\n            "party": "-",\n            "event": "lab appointment"\n          }\n        ],\n        ' \
                                        '"column_names": [\n' \
                                        '          "event",\n          "time",\n          "date",\n          "room",\n          "agenda",\n' \
                                        '          "party"\n        ],\n        "kb_title": "calendar"\n      },\n      "task": {\n' \
                                        '        "intent": "schedule"\n      },\n      "uuid": "e6a4e9dc-a952-47dc-bb7f-3586cdb1c3ff"\n    }'
    elif topic == "weather":
        # TODO: Switch between days etc
        try:
            low_temp = str(int(knowledgebase['temperature']) - 10)
            high_temp = str(int(knowledgebase['temperature']) + 10)
        except ValueError:
            low_temp = str(int(knowledgebase['temperature'][:-1]) - 10)
            high_temp = str(int(knowledgebase['temperature'][:-1]) + 10)
        dialogue_json += '\n    ],\n    "scenario": {\n      "kb": {\n        "items": [\n          {\n' \
            '            "monday": "hot, low of 90F, high of 100F",\n            ' \
            '"tuesday": "foggy, low of 30F, high of 50F",\n            ' \
            '"friday": "stormy, low of 50F, high of 60F",\n            ' \
            '"wednesday": "cloudy, low of 90F, high of 100F",\n            ' \
            '"thursday": "overcast, low of 80F, high of 100F",\n            ' \
            '"sunday": "overcast, low of 20F, high of 30F",\n            ' \
            '"location": "danville",\n            ' \
            '"saturday": "dry, low of 80F, high of 90F",\n            ' \
            '"today": "monday"\n          },\n' \
            '          {\n            "monday": "' + knowledgebase['weather_attribute'] +\
            ', low of ' + low_temp + 'F, high of ' + high_temp + 'F",\n            ' \
            '"tuesday": "cloudy, low of 40F, high of 60F",\n            "friday": "clear skies, low of 20F, high of 30F",\n            ' \
            '"wednesday": "foggy, low of 90F, high of 100F",\n            "thursday": "cloudy, low of 90F, high of 100F",\n            ' \
            '"sunday": "stormy, low of 90F, high of 100F",\n            "location": "' + knowledgebase['location'] +\
            '",\n            "saturday": "drizzle, low of 30F, high of 40F",\n            "today": "monday"\n          },\n' \
            '          {\n            "monday": "rain, low of 80F, high of 100F",\n            ' \
            '"tuesday": "cloudy, low of 40F, high of 60F",\n            "friday": "clear skies, low of 20F, high of 30F",\n            ' \
            '"wednesday": "foggy, low of 90F, high of 100F",\n            "thursday": "cloudy, low of 90F, high of 100F",\n            ' \
            '"sunday": "stormy, low of 90F, high of 100F",\n            "location": "alhambra",\n            "saturday": "drizzle, low of 30F, high of 40F",\n' \
            '            "today": "monday"\n          },\n' \
            '          {\n            "monday": "snow, low of 40F, high of 50F",\n            "tuesday": "cloudy, low of 90F, high of 100F",\n            ' \
            '"friday": "stormy, low of 70F, high of 90F",\n            "wednesday": "dry, low of 40F, high of 60F",\n            ' \
            '"thursday": "rain, low of 40F, high of 60F",\n            "sunday": "warm, low of 50F, high of 70F",\n            ' \
            '"location": "inglewood",\n            "saturday": "warm, low of 70F, high of 90F",\n            "today": "monday"\n          },\n' \
            '          {\n            "monday": "rain, low of 80F, high of 100F",\n            "tuesday": "cloudy, low of 40F, high of 50F",\n            ' \
            '"friday": "hail, low of 90F, high of 100F",\n            "wednesday": "windy, low of 30F, high of 40F",\n            ' \
            '"thursday": "rain, low of 40F, high of 50F",\n            "sunday": "overcast, low of 50F, high of 70F",\n            ' \
            '"location": "durham",\n            "saturday": "hail, low of 70F, high of 80F",\n            "today": "monday"\n          },\n' \
            '          {\n            "monday": "clear skies, low of 40F, high of 50F",\n            "tuesday": "cloudy, low of 20F, high of 40F",\n            ' \
            '"friday": "warm, low of 80F, high of 90F",\n            "wednesday": "drizzle, low of 80F, high of 100F",\n            ' \
            '"thursday": "clear skies, low of 60F, high of 70F",\n            "sunday": "hail, low of 90F, high of 100F",\n            ' \
            '"location": "san francisco",\n            "saturday": "foggy, low of 80F, high of 90F",\n            "today": "monday"\n          },\n' \
            '          {\n            "monday": "clear skies, low of 90F, high of 100F",\n            "tuesday": "dry, low of 70F, high of 90F",\n            ' \
            '"friday": "foggy, low of 60F, high of 80F",\n            "wednesday": "stormy, low of 20F, high of 40F",\n            ' \
            '"thursday": "dry, low of 80F, high of 100F",\n            "sunday": "raining, low of 20F, high of 40F",\n            ' \
            '"location": "compton",\n            "saturday": "raining, low of 70F, high of 80F",\n            "today": "monday"\n          },\n' \
            '          {\n            "monday": "dry, low of 70F, high of 90F",\n            "tuesday": "overcast, low of 90F, high of 100F",\n            ' \
            '"friday": "hail, low of 70F, high of 90F",\n            "wednesday": "hail, low of 20F, high of 40F",\n            ' \
            '"thursday": "dry, low of 80F, high of 90F",\n            "sunday": "humid, low of 90F, high of 100F",\n            ' \
            '"location": "brentwood",\n            "saturday": "dry, low of 20F, high of 40F",\n            "today": "monday"\n          }\n        ],\n' \
            '        "column_names": [\n          "location",\n          "monday",\n          "tuesday",\n          "wednesday",\n          "thursday",\n          ' \
            '"friday",\n          "saturday",\n          "sunday",\n          "today"\n        ],\n        ' \
            '"kb_title": "weekly forecast"\n      },\n      ' \
            '"task": {\n        "intent": "weather"\n      },\n      "uuid": "db03e551-eca1-44ec-af26-2959190e2316"\n    }'
    elif topic == "navigate":
        dialogue_json += '\n    ],\n    "scenario": {\n      "kb": {\n        "items": [\n          ' \
                                        '{\n            "distance": "5 miles",\n            "traffic_info": "moderate traffic",\n' \
                                        '            "poi_type": "gas station",\n            "address": "783 Arcadia Pl",\n' \
                                        '            "poi": "Chevron"\n          },\n          ' \
                                        '{\n            "distance": "' + str(knowledgebase['distance']) + 'miles",\n' \
                                        '            "traffic_info": "' + knowledgebase['traffic_info'] + '",\n            ' \
                                        '"poi_type": "' + knowledgebase['poi_type'] + '",\n' \
                                        '            "address": "' + knowledgebase['poi_address'] + '",\n            ' \
                                        '"poi": "' + knowledgebase['poi_poi'] + '"\n' \
                                        '          },\n          ' \
                                        '{\n            "distance": "5 miles",\n' \
                                        '            "traffic_info": "no traffic",\n            "poi_type": "shopping center",\n' \
                                        '            "address": "383 University Ave",\n            "poi": "Town and Country"\n' \
                                        '          },\n          ' \
                                        '{\n            "distance": "5 miles",\n            "traffic_info": "no traffic",\n' \
                                        '            "poi_type": "friends house",\n            "address": "864 Almanor Ln",\n            "poi": "jacks house"\n          },\n          ' \
                                        '{\n            "distance": "6 miles",\n            "traffic_info": "heavy traffic",\n            "poi_type": "home",\n' \
                                        '            "address": "5671 barringer street",\n            "poi": "home"\n          },\n          ' \
                                        '{\n            "distance": "4 miles",\n            "traffic_info": "no traffic",\n            ' \
                                        '"poi_type": "rest stop",\n            "address": "657 Ames Ave",\n            "poi": "The Clement Hotel"\n          },\n          ' \
                                        '{\n            "distance": "1 miles",\n            "traffic_info": "heavy traffic",\n            "poi_type": "grocery store",\n' \
                                        '            "address": "638 Amherst St",\n            "poi": "Sigona Farmers Market"\n          },\n          ' \
                                        '{\n            "distance": "6 miles",\n            "traffic_info": "no traffic",\n            "poi_type": "chinese restaurant",\n' \
                                        '            "address": "830 Almanor Ln",\n            "poi": "tai pan"\n          }\n        ],\n' \
                                        '        "column_names": [\n          "poi",\n          "poi_type",\n          "address",\n          "distance",\n          "traffic_info"\n        ],\n' \
                                        '        "kb_title": "location information"\n      },\n      "task": {\n        "intent": "navigate"\n      },\n      "uuid": "d70e1162-8bcb-4f28-9d14-078f90974351"\n    }'
    return dialogue_json


def sub_tokens(list_lines):
    for i, lines in enumerate(list_lines):
        for k, line in enumerate(lines):
            dialogue = check_dialogue_turns(line)
            task_type_scores = [0, 0, 0]
            kb = {"event": "taking medicine", "time": "1pm", "date": "monday", "party": "sales team", "room": "conference room 100",
                  "agenda": "go over budget", "location": "oakland", "weekly_time": "today", "temperature": 20,
                  "weather_attribute": "lowest temperature", "traffic_info": "no traffic", "poi_type": "chinese restaurant",
                  "poi_address": "593 Arrowhead Way", "poi_poi": "Chef Chu's", "distance": 1}
            last_substitutes = {}
            if to_sub:
                for turn_index, turn in enumerate(dialogue):
                    # TODO: Save token for usage in all turns
                    for token in dictionary.items():
                        if re.match("(.*)<" + token[0] + ">(.*)", turn):
                            turn, kb, last_substitutes = sub_token(token[0], turn, kb, last_substitutes)
                            if token[0] == "event" or token[0] == "time" or token[0] == "date" or token[0] == "party"\
                                    or token[0] == "room" or token[0] == "agenda":
                                task_type_scores[0] += 1
                            if token[0] == "location" or token[0] == "weekly_time" or token[0] == "temperature"\
                                    or token[0] == "weather_attribute" or token[0] == "date":
                                task_type_scores[1] += 1
                            if token[0] == "traffic_info" or token[0] == "poi_address" or token[0] == "poi_poi"\
                                    or token[0] == "poi_type" or token[0] == "distance":
                                task_type_scores[2] += 1
                    dialogue[turn_index] = turn
            task_type = "unknown"
            if task_type_scores.index(max(task_type_scores)) == 0:
                task_type = "schedule"
            elif task_type_scores.index(max(task_type_scores)) == 1:
                task_type = "weather"
            elif task_type_scores.index(max(task_type_scores)) == 2:
                task_type = "navigate"
            if k == len(lines) - 1 and i == len(list_lines) - 1:
                dialogue = add_kb(dialogue, task_type, kb)
            else:
                dialogue = add_kb(dialogue, task_type, kb) + "\n  },\n  {\n    "
            lines[k] = dialogue
        list_lines[i] = lines
    return list_lines


def save_lines(lines, name_suffix, mode):
    with open(input_file + mode + name_suffix + "-" + str(start_temp) + ".txt", 'w') as out_file:
        for line in lines:
            out_file.writelines(line)


if __name__ == "__main__":
    load_kb()
    for mode in ["weather", "schedule", "navigate"]:
        input_lines = load_lines(mode)
        for start_temp in range(0, 11, 1):
            start_temp = start_temp / 10
            if start_temp == 1.0:
                end_temp = 0.0
            else:
                end_temp = start_temp + 0.1
            end_temp = end_temp.__round__(1)
            preproc_lines = preprocess_lines(input_lines, str(start_temp), str(end_temp), 10)
            save_lines(preproc_lines, "_preproc", mode)
            subbed_lines = sub_tokens(preproc_lines)
            save_lines(subbed_lines, "_subbed", mode)
            subbed_lines_json = "[\n  {\n    "
            for index, subbed_dialogue in enumerate(subbed_lines):
                if index == 0:
                    subbed_lines_json += ""  # "{\n    "
                else:
                    subbed_lines_json += ",\n  {\n    "
                for subbed_ln in subbed_dialogue:
                    subbed_lines_json += subbed_ln  # + "\n  }"
                subbed_lines_json += "\n  }"
            subbed_lines_json += "\n]"
            # TODO: Convert from string to bytes-like object
            numpy.savetxt(input_file + mode + '_subbed-' + str(start_temp) + '.json', [subbed_lines_json], fmt='%s')
