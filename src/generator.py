import tensorflow as tf
import numpy as np
from src.util import get_inputs_int, tensor_size


def sample_Z(m, n, vocab_len):
    samples = list(np.random.uniform(-1.0, 1.0, size=[m, n]))
    inputs_int = get_inputs_int()
    # TODO: Fix this, random number should be somewhere in the vector space, need to find max/min for this
    for index, _ in enumerate(samples):
        samples[index][0] = np.random.randint(0, len(inputs_int))
        samples[index][-1] = np.random.uniform(10, 30)
    # for index, sample in enumerate(samples):
    #     samples[index][0] = int(samples[index][0])
    #     samples[index][1] = int(samples[index][1])
    # return np.array(samples)
    # for index, sample in enumerate(samples):
    #     for i in range(1, 21):
    #         samples[index][i] = inputs_int[sample[0]][i - 1]
    return np.append(np.array(samples), np.array(reversed(samples)))  # np.random.uniform(-1., 1., size=[m, n])


def generator(Z, hsize=[16, 16], reuse=False):
    with tf.variable_scope("GAN/Generator", reuse=reuse):
        h1 = tf.layers.dense(Z, hsize[0], activation=tf.nn.leaky_relu)
        h2 = tf.layers.dense(h1, hsize[1], activation=tf.nn.leaky_relu)
        out = tf.layers.dense(h2, tensor_size)

    return out
