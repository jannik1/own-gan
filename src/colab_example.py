from colabrnn.rnn import CharGen

file_name = input("Please specify file_name") # "weights-improved-16-0.27.hdf5"
# file_name= "weights-generated__schedule.txt-improved-25-0.41.hdf5"
if file_name[19] == "w":
    char_gen = CharGen(weights_filepath='models/' + file_name,  # specify correct filename 
                       vocab_filepath=file_name[19:26] + '_vocabulary.json',  # specify correct filename
                       config_filepath=file_name[19:26] + '_config.json')  # specify correct filename 
elif file_name[19] == "s" or file_name[19] == "n":
    char_gen = CharGen(weights_filepath='models/' + file_name,  # specify correct filename 
                       vocab_filepath=file_name[19:27] + '_vocabulary.json',  # specify correct filename
                       config_filepath=file_name[19:27] + '_config.json')  # specify correct filename 

starting_strings = ["please ", "what ", "can you ", "tell ", "check ", "will ", "what's ", "is ", "hey car, ", "hello "]
for i in range(10):
    for k in range(10):
        char_gen.generate(temps=[k/10], gen_text_length=50000,
                          prefix='driver0: ' + starting_strings[i])
