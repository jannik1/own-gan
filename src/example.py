import numpy

import tensorflow as tf
from src.generator import generator
from src.util import sample_data, load_vocab_inputs, get_vocab, mode, kb, get_inputs_int, get_word_to_vec, word_based, \
    tensor_size
from src.discriminator import discriminator
from src.generator import sample_Z


def run_example(local_g_plot):
    # global vocabulary
    # try:
    #     vocabulary
    # except NameError:
    #     vocabulary = get_vocab()
    global word_to_vec
    try:
        word_to_vec
    except NameError:
        word_to_vec = get_word_to_vec()
    sentence = 20 * [0]
    words = 20 * [""]
    inputs_int = get_inputs_int()
    if word_based:
        sentences = 1 * [20 * [""]]
    else:
        sentences = len(inputs_int) * [20 * [""]]
    # sentence_numbers = 20 * [0]
    # for dialog_pos, sentence[0], sentence[1], sentence[2], sentence[3], sentence[4], sentence[5],\
        # sentence[6], sentence[7], sentence[8], sentence[9], sentence[10], sentence[11], sentence[12], sentence[13],\
        # sentence[14], sentence[15], sentence[16], sentence[17], sentence[18], sentence[19] in local_g_plot:
    if word_based:
        sentence_vecs = 20 * [385 * [0]]
        for index in range(len(local_g_plot)):
            try:
                sentence_index = int(local_g_plot[index][0].__round__())
                if 0 <= sentence_index < 20:
                    sentence_vecs[sentence_index] = local_g_plot[index][1:386]
            except:
                print("ERROR!")
        for word_index in range(20):
            max_similarity = -1
            most_similar_word = "NAN"
            word_vec = sentence_vecs[word_index][:-1]
            word_vec_norm = sentence_vecs[word_index][-1:]
            for word, (vec, vec_norm) in word_to_vec.items():
                similarity = numpy.dot(word_vec, vec) / (vec_norm * word_vec_norm[0])
                if similarity > max_similarity and word != "<pad>" and word != "<unk>" and word != "<eos>":
                    most_similar_word = word
                    max_similarity = similarity
            words[word_index] = most_similar_word
            # if not (0 <= int(word.__round__()) < vocabulary.size()):
            #     sentence[word_index] = 0
            sentences[0] = words  # +=
    else:
        for index in range(len(local_g_plot)):
            dialog_pos = local_g_plot[index][0]
            sentence_vecs = 20 * [385 * [0]]
            for loc_i in range(len(sentence_vecs)):
                try:
                    sentence_vecs[loc_i] = local_g_plot[index][1 + (385 * loc_i):1 + (385 * (loc_i + 1))]
                except:
                    print("ERROR!")
                    print("INDEX: " + str(index))
                    print("loc_i: " + str(loc_i))
                    print("START: " + str(1 + (385 * loc_i)))
                    print("END: " + str(1 + (385 * (loc_i + 1))))
            if 0 <= int(dialog_pos.__round__()) < len(inputs_int):
                in_span = True
                for word_index in range(20):
                    max_similarity = -1
                    most_similar_word = "NAN"
                    word_vec = sentence_vecs[word_index][:-1]
                    word_vec_norm = sentence_vecs[word_index][-1:]
                    for word, (vec, vec_norm) in word_to_vec.items():
                        similarity = numpy.dot(word_vec, vec) / (vec_norm * word_vec_norm)
                        if similarity > max_similarity:
                            most_similar_word = word
                            max_similarity = similarity
                    words[word_index] = most_similar_word
                    # if not (0 <= int(word.__round__()) < vocabulary.size()):
                    #     sentence[word_index] = 0
                if in_span:
                    sentences[int(dialog_pos.__round__())] = words  # +=
        # sentence_numbers[int(word_pos.__round__())] += 1
    # for word_index, word in enumerate(sentence):
    #     sentence[word_index] = int((sentence[word_index] / sentence_numbers[word_index]).__float__())
    dialogues = []
    for dialogue in sentences:
        # dialogue = ' '.join(vocabulary.int_to_string(dialogue))
        dialogue = ' '.join(dialogue)
        dialogues.append(dialogue)
    return dialogues


if __name__ == "__main__":
    batch_size = 200
    nd_steps = 10
    ng_steps = 10

    X = tf.placeholder(tf.float32, [None, tensor_size])
    Z = tf.placeholder(tf.float32, [None, tensor_size])

    G_sample = generator(Z, hsize=[tensor_size,tensor_size])
    r_logits, r_rep = discriminator(X, hsize=[tensor_size,tensor_size])
    f_logits, g_rep = discriminator(G_sample, hsize=[tensor_size,tensor_size], reuse=True)

    saver = tf.train.Saver()
    sess = tf.Session()
    i = 4610
    saver.restore(sess, "../weights/model_" + mode + "_" + str(i) + ".ckpt")

    load_vocab_inputs()
    vocabulary = get_vocab()

    x_plot = sample_data(n=batch_size)

    for i in range(10):
        X_batch = sample_data(n=batch_size)
        Z_batch = sample_Z(batch_size, tensor_size, vocabulary.size())

        g_plot = sess.run(G_sample, feed_dict={Z: Z_batch})
        dialogues = run_example(g_plot)
        for dialogue in dialogues:
            print(dialogue)
